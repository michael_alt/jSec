CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	active TINYINT(1) NOT NULL DEFAULT 1,
	type INT NOT NULL DEFAULT 1,
	username VARCHAR(255) NOT NULL,
	encoded_password VARCHAR(255) NULL,
	display_name VARCHAR(255) NOT NULL,
	mail VARCHAR(255) NOT NULL,
	avatar TINYINT NOT NULL DEFAULT 0,
	language VARCHAR(2) NULL,
	lockout_attempts INT NOT NULL DEFAULT 0,
	lockout_time TIMESTAMP NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id),
	UNIQUE KEY `index_users_on_username` (username),
	UNIQUE KEY `index_users_on_mail` (mail)
);

CREATE TABLE users_roles(
	user_id INT NOT NULL,
	role_id INT NOT NULL,
	UNIQUE KEY `index_users_roles_on_user_id_role_id` (user_id,role_id)
);

CREATE TABLE users_sessions(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	secret VARCHAR(36) NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	expires_at TIMESTAMP NOT NULL,
	device_ip VARCHAR(255) NOT NULL,
	device_type VARCHAR(20) NULL,
	device_os VARCHAR(255) NULL,
	device_browser VARCHAR(255) NULL,
	PRIMARY KEY(id),
	UNIQUE KEY `index_users_sessions_on_secret` (secret)
);