CREATE TABLE settings(
	id INT NOT NULL DEFAULT 1,
	session_secret VARCHAR(64) NOT NULL,
	session_timeout INT NOT NULL DEFAULT 86400,
	password_policy_level INT NOT NULL DEFAULT 1,
	PRIMARY KEY(id)
);