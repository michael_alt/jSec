package org.jsec.api.v1.config.handlers;

import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

/**
 * This class is used to gzip compress all api responses.
 * 
 * @author alt
 *
 */
@Provider
public class ApiCompressionHandler implements WriterInterceptor {

	@Override
	public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {

		GZIPOutputStream os = new GZIPOutputStream(context.getOutputStream());

		context.getHeaders().putSingle("Content-Encoding", "gzip");
		context.setOutputStream(os);
		context.proceed();
		return;
	}

}