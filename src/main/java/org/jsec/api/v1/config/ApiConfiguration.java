package org.jsec.api.v1.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.jsec.core.exceptions.EntityPersistenceException;

/**
 * This class is used to configure the REST API v1 of the application.
 * 
 * @author alt
 *
 */
@ApplicationPath("/")
public class ApiConfiguration extends ResourceConfig {

	/**
	 * The default constructor of the class.
	 * 
	 * All configurations for the REST API are set here.
	 * 
	 * @throws EntityPersistenceException
	 *             Will be thrown if the application settings could not be loaded
	 */
	public ApiConfiguration() {

		// Packages to scan for jersey components
		packages(true, "org.jsec.api.v1");

		// Register jersey features
		register(MultiPartFeature.class);

		// Disable automatic WADL generation
		property(ServerProperties.WADL_FEATURE_DISABLE, true);
	}

}