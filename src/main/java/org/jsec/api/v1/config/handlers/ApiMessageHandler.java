package org.jsec.api.v1.config.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.jsec.api.v1.config.security.ApiSecurityRole;
import org.jsec.api.v1.config.security.ApiSecurityRole.AdministratorView;
import org.jsec.api.v1.config.security.ApiSecurityRole.AuthenticatedView;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

/**
 * This class is used to read and write JSON data from requests and responses.
 * 
 * @author alt
 *
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ApiMessageHandler implements MessageBodyReader<Object>, MessageBodyWriter<Object> {

	private ObjectMapper mapper;

	@Context
	private SecurityContext context;

	public ApiMessageHandler() {

		mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.USE_DEFAULTS);
		mapper.disable(MapperFeature.AUTO_DETECT_CREATORS, MapperFeature.AUTO_DETECT_FIELDS,
				MapperFeature.AUTO_DETECT_GETTERS, MapperFeature.AUTO_DETECT_IS_GETTERS);
		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		mapper.registerModule(new Hibernate5Module());
	}

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return mapper.canSerialize(type);
	}

	@Override
	public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> headers, InputStream inputStream) throws IOException {

		return mapper.readValue(inputStream, type);
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return mapper.canSerialize(type);
	}

	@Override
	public long getSize(Object t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return -1;
	}

	@Override
	public void writeTo(Object data, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> headers, OutputStream outputStream) throws IOException {

		if (context != null && context.getUserPrincipal() != null) {
			if (context.isUserInRole(ApiSecurityRole.ADMINISTRATOR)) {
				mapper.writerWithView(AdministratorView.class).writeValue(outputStream, data);
			} else {
				mapper.writerWithView(AuthenticatedView.class).writeValue(outputStream, data);
			}
		} else {
			mapper.writeValue(outputStream, data);
		}

		outputStream.flush();
	}

}