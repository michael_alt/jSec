package org.jsec.api.v1.config.handlers.error.entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if entities could not be found.
 * 
 * @author alt
 *
 */
@Provider
public class EntityNotFoundExceptionHandler implements ExceptionMapper<EntityNotFoundException> {

	private static final Logger log = LogManager.getLogger(EntityNotFoundExceptionHandler.class);

	public EntityNotFoundExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(EntityNotFoundException error) {

		log.error("API.EntityNotFoundException", error);

		ApplicationError item = new ApplicationError();
		if (error.getError() != null) {
			item = error.getError();
		} else {
			item = new ApplicationError("entityNotFoundException", error.getMessage());
		}

		return Response.status(404).type(MediaType.APPLICATION_JSON).entity(item).build();
	}

}