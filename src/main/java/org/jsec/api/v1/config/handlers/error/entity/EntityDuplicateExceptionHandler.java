package org.jsec.api.v1.config.handlers.error.entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if an duplicate entity was found.
 * 
 * @author alt
 *
 */
@Provider
public class EntityDuplicateExceptionHandler implements ExceptionMapper<EntityDuplicateException> {

	private static final Logger log = LogManager.getLogger(EntityDuplicateException.class);

	public EntityDuplicateExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(EntityDuplicateException error) {

		log.error("API.EntityDuplicateException", error);

		ApplicationError item = new ApplicationError();
		if (error.getError() != null) {
			item = error.getError();
		} else {
			item = new ApplicationError("entityDuplicateException", error.getMessage());
		}

		return Response.status(400).type(MediaType.APPLICATION_JSON).entity(item).build();
	}

}