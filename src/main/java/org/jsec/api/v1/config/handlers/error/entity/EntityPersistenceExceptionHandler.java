package org.jsec.api.v1.config.handlers.error.entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if entities could not be persisted.
 * 
 * @author alt
 *
 */
@Provider
public class EntityPersistenceExceptionHandler implements ExceptionMapper<EntityPersistenceException> {

	private static final Logger log = LogManager.getLogger(EntityPersistenceExceptionHandler.class);

	public EntityPersistenceExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(EntityPersistenceException error) {

		log.error("API.EntityPersistenceException", error);

		ApplicationError item = new ApplicationError();
		if (error.getError() != null) {
			item = error.getError();
		} else {
			item = new ApplicationError("entityPersistenceException", error.getMessage());
		}

		return Response.status(500).type(MediaType.APPLICATION_JSON).entity(item).build();
	}

}