package org.jsec.api.v1.config.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * This class represents permission roles for the access to API resources.
 * 
 * @author alt
 *
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface ApiSecurityRole {

	public static final String AUTHENTICATED = "AUTHENTICATED";
	public static final String ADMINISTRATOR = "ADMINISTRATOR";

	String[] value();

	public interface AuthenticatedView {
	}

	public interface AdministratorView extends AuthenticatedView {
	}

}