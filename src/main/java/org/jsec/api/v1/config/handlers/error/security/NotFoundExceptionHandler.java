package org.jsec.api.v1.config.handlers.error.security;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if an not existing resource was requested.
 * 
 * @author alt
 *
 */
@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

	private static final Logger log = LogManager.getLogger(NotFoundExceptionHandler.class);

	public NotFoundExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(NotFoundException error) {

		log.error("API.NotFoundException", error);
		return Response.status(404).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("notFound", "Resource could not be found.")).build();
	}

}