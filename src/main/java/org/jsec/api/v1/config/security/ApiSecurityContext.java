package org.jsec.api.v1.config.security;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;

/**
 * Custom security context for the jersey application. This is used for user
 * permission management.
 * 
 * @author alt
 *
 */
public class ApiSecurityContext implements SecurityContext {

	private User user;

	public ApiSecurityContext(User user) {
		this.user = user;
	}

	@Override
	public Principal getUserPrincipal() {
		return this.user;
	}

	@Override
	public boolean isUserInRole(String role) {

		for (Role userRole : user.getRoles()) {
			if (userRole.getUniqueName().trim().equalsIgnoreCase(role.trim())) {
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean isSecure() {
		return true;
	}

	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

}