package org.jsec.api.v1.config.handlers.error.security;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages for requests with wrong method.
 * 
 * @author alt
 *
 */
@Provider
public class NotAllowedExceptionHandler implements ExceptionMapper<NotAllowedException> {

	private static final Logger log = LogManager.getLogger(NotAllowedException.class);

	public NotAllowedExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(NotAllowedException error) {

		log.error("API.NotAllowedException", error);
		return Response.status(405).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("invalidOperation", "Operation not allowed.")).build();
	}

}