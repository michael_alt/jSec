package org.jsec.api.v1.config.handlers.error.parse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

import com.fasterxml.jackson.core.JsonParseException;

/**
 * Handles error messages for JSON parse erros.
 * 
 * @author alt
 *
 */
@Provider
public class JsonParseExceptionHandler implements ExceptionMapper<JsonParseException> {

	private static final Logger log = LogManager.getLogger(JsonParseExceptionHandler.class);

	public JsonParseExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(JsonParseException error) {

		log.error("API.JsonParseException", error);
		return Response.status(400).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("parseError", error.getMessage())).build();
	}

}