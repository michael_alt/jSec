package org.jsec.api.v1.config.security;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.services.user.UserSessionService;

/**
 * This class is used to manage access to the REST API.
 * 
 * @author alt
 *
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class ApiSecurityFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	@Context
	private HttpServletRequest request;

	private UserSessionService sessionService;

	/**
	 * Default constructor of the class.
	 */
	public ApiSecurityFilter() {
		this.sessionService = new UserSessionService();
	}

	/**
	 * Filters all requests to check make sure the user is authenticated.
	 */
	@Override
	public void filter(ContainerRequestContext context) throws IOException {

		Method method = this.resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(ApiSecurityRole.class)) {

			User authUser = checkAuthToken(context);
			ApiSecurityContext securityContext = new ApiSecurityContext(authUser);

			if (authUser == null) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}

			// Check if the user has sufficient roles
			if (method.isAnnotationPresent(ApiSecurityRole.class)
					&& !securityContext.isUserInRole(ApiSecurityRole.ADMINISTRATOR)) {

				String[] permissions = method.getAnnotation(ApiSecurityRole.class).value();
				boolean hasPermissions = false;
				for (String permission : permissions) {
					if (permission.equals(ApiSecurityRole.AUTHENTICATED) || securityContext.isUserInRole(permission)) {
						hasPermissions = true;
						break;
					}
				}

				if (!hasPermissions) {
					throw new ForbiddenException();
				}
			}

			context.setSecurityContext(securityContext);
		}
	}

	/**
	 * Checks if a valid auth token was provided and returns the user account.
	 * 
	 * @param context
	 *            The context of the request
	 * @return The user account
	 */
	private User checkAuthToken(ContainerRequestContext context) {

		String authorizationHeader = context.getHeaderString(HttpHeaders.AUTHORIZATION);
		String accessToken = null;

		if (authorizationHeader != null) {
			accessToken = authorizationHeader.substring("Bearer".length()).trim();
		}

		if (accessToken == null || !this.sessionService.checkToken(accessToken)) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		User user = this.sessionService.getUserByToken(accessToken);
		if (user == null || !user.isActive()) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		return this.sessionService.getUserByToken(accessToken);

	}

}