package org.jsec.api.v1.config.handlers.error.entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if an entity validation error occurs.
 * 
 * @author alt
 *
 */
@Provider
public class EntityValidationExceptionHandler implements ExceptionMapper<EntityValidationException> {

	private static final Logger log = LogManager.getLogger(EntityValidationExceptionHandler.class);

	public EntityValidationExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(EntityValidationException error) {

		log.error("API.EntityValidationException", error);

		ApplicationError item = new ApplicationError();
		if (error.getError() != null) {
			item = error.getError();
		} else {
			item = new ApplicationError("entityValidationException", error.getMessage());
		}

		return Response.status(400).type(MediaType.APPLICATION_JSON).entity(item).build();
	}

}