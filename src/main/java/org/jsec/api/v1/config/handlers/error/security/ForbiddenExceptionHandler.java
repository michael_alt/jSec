package org.jsec.api.v1.config.handlers.error.security;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages if the user has no access to the requested resource.
 * 
 * @author alt
 *
 */
@Provider
public class ForbiddenExceptionHandler implements ExceptionMapper<ForbiddenException> {

	private static final Logger log = LogManager.getLogger(ForbiddenExceptionHandler.class);

	public ForbiddenExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(ForbiddenException error) {

		log.error("API.ForbiddenException", error);
		return Response.status(403).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("forbidden", "You are not allowed to execute this operation.")).build();
	}

}