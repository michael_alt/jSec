package org.jsec.api.v1.config.handlers.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages that are not mapped by specific handlers.
 * 
 * @author alt
 *
 */
@Provider
public class ApiExceptionHandler implements ExceptionMapper<Exception> {

	private static final Logger log = LogManager.getLogger(ApiExceptionHandler.class);

	public ApiExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(Exception error) {

		log.error("serverError", error);
		return Response.status(500).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("serverError", "Request could not be processed.")).build();
	}

}