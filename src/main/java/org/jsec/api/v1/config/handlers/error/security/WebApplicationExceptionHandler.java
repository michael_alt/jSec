package org.jsec.api.v1.config.handlers.error.security;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.util.error.ApplicationError;

/**
 * Handles error messages for unauthenticated requests.
 * 
 * @author alt
 *
 */
@Provider
public class WebApplicationExceptionHandler implements ExceptionMapper<WebApplicationException> {

	private static final Logger log = LogManager.getLogger(WebApplicationExceptionHandler.class);

	public WebApplicationExceptionHandler() {
		// The default constructor of the class
	}

	@Override
	public Response toResponse(WebApplicationException error) {

		log.error("API.WebApplicationException", error);
		return Response.status(401).type(MediaType.APPLICATION_JSON)
				.entity(new ApplicationError("unauthorized", "You are not authorized.")).build();
	}

}