package org.jsec.api.v1.resources.auth.token;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.jsec.core.data.entities.user.AuthToken;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.services.user.UserService;
import org.jsec.core.data.services.user.UserSessionService;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.security.auth.AuthProvider;
import org.jsec.core.security.auth.database.DatabaseAuthProvider;
import org.jsec.core.util.error.ApplicationError;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This class represents the auth api endpoint. It is the only resource that is
 * accessible without authentication.
 * 
 * @author alt
 *
 */
@Path("auth/token")
@Produces(MediaType.APPLICATION_JSON)
public class TokenAuthResource {

	@Context
	private HttpServletRequest request;

	@Context
	private SecurityContext securityContext;

	private UserService userService;
	private UserSessionService sessionService;

	/**
	 * Default constructor of the class.
	 */
	public TokenAuthResource() {
		this.userService = new UserService();
		this.sessionService = new UserSessionService();
	}

	/**
	 * Authenticates an user account and returns an valid JSON Web Token if the user
	 * is authenticated.
	 * 
	 * @param json
	 *            The login parameters
	 * @return The new JSON web token
	 */
	@POST
	public Response login(JsonNode json) throws EntityValidationException {

		try {

			String username = json.get("username").asText();
			String password = json.get("password").asText();

			// Authenticate users
			User authUser = this.userService.getUser(username);
			if (authUser != null) {

				// Check if the user has to many login attempts
				if (isLockoutActive(authUser)) {
					throw new EntityValidationException(new ApplicationError("lockoutAuthentication",
							"To many login attempts, account is locked until: " + authUser.getLockoutTime()));
				}

				AuthProvider authProvider = null;
				if (authUser.getType() == 1) {
					authProvider = new DatabaseAuthProvider();
				}

				if (authProvider != null && authProvider.authenticate(username, password)) {
					unsetLockout(authUser);
					return Response.status(200).entity(issueToken(authUser)).build();
				} else {
					setLockout(authUser);
					throw new EntityValidationException(
							new ApplicationError("invalidAuthentication", "Invalid username or password."));
				}
			} else {
				throw new EntityValidationException(
						new ApplicationError("invalidAuthentication", "Invalid username or password."));
			}
		} catch (NullPointerException | EntityPersistenceException | EntityDuplicateException
				| UnknownHostException e) {
			e.printStackTrace();
			throw new EntityValidationException(
					new ApplicationError("invalidAuthentication", "Invalid username or password."));
		}
	}

	/**
	 * Check if the user has an lockout marker.
	 * 
	 * @param authUser
	 *            The user account
	 * @return True if lockout is active
	 */
	private boolean isLockoutActive(User authUser) {

		if (authUser.getLockoutTime() != null && authUser.getLockoutTime().getTime() >= new Date().getTime()) {
			return true;
		}

		return false;
	}

	/**
	 * Adds the lockout marker to the user account.
	 * 
	 * @param authUser
	 *            The user account
	 */
	private void setLockout(User authUser)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		authUser.setLockoutAttempts(authUser.getLockoutAttempts() + 1);
		if (authUser.getLockoutAttempts() == 3) {
			authUser.setLockoutTime(new Date(new Date().getTime() + (15 * 60 * 1000)));
		} else if (authUser.getLockoutAttempts() == 6) {
			authUser.setLockoutTime(new Date(new Date().getTime() + (30 * 60 * 1000)));
		} else if (authUser.getLockoutAttempts() >= 9) {
			authUser.setLockoutTime(new Date(new Date().getTime() + (60 * 60 * 1000)));
		}

		this.userService.update(authUser);
	}

	/**
	 * Removes the lockout marker from the user account.
	 * 
	 * @param authUser
	 *            The user account
	 */
	private void unsetLockout(User authUser)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		if (authUser.getLockoutAttempts() > 0) {
			authUser.setLockoutAttempts(0);
			authUser.setLockoutTime(null);
			this.userService.update(authUser);
		}
	}

	/**
	 * Issues a new token for the specified user account.
	 * 
	 * @param user
	 *            The user account
	 * @return The new auth token
	 */
	private AuthToken issueToken(User user)
			throws UnknownHostException, EntityValidationException, EntityPersistenceException {

		String remoteAddress = request.getRemoteHost();

		InetAddress address = InetAddress.getByName(remoteAddress);
		remoteAddress = address.getHostName();

		String userAgentString = request.getHeader("User-Agent");
		return this.sessionService.issueToken(user, remoteAddress, userAgentString);
	}

}