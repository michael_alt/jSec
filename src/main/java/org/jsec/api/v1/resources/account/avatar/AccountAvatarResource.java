package org.jsec.api.v1.resources.account.avatar;

import java.io.InputStream;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jsec.api.v1.config.security.ApiSecurityRole;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.services.user.UserService;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.storage.user.UserStorageProvider;
import org.jsec.core.util.error.ApplicationError;

/**
 * This class represents the account avatar api endpoint.
 * 
 * @author alt
 *
 */
@Path("account/avatar/")
@Produces(MediaType.APPLICATION_JSON)
public class AccountAvatarResource {

	@Context
	private SecurityContext securityContext;

	private UserService userService;
	private UserStorageProvider userStorage;

	/**
	 * Default constructor of the class.
	 */
	public AccountAvatarResource() {
		this.userService = new UserService();
		this.userStorage = new UserStorageProvider();
	}

	/**
	 * Adds or updates the avatar image of the current user.
	 * 
	 * @param inputStream
	 *            The file upload stream
	 * @return Success or an error message
	 */
	@POST
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response updateAvatar(@FormDataParam("file") InputStream inputStream)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		User authUser = (User) securityContext.getUserPrincipal();

		if (this.userStorage.storeUserAvatar(authUser, inputStream)) {
			authUser.setAvatar(true);
			this.userService.update(authUser);
			return Response.status(201).build();
		} else {
			throw new EntityValidationException(
					new ApplicationError("userAvatarCreate", "The avatar of the user could not be saved."));
		}
	}

	/**
	 * Removes the avatar image of the current user.
	 * 
	 * @return Nothing (204) on success or an error message
	 */
	@DELETE
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response removeAvatar()
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		User authUser = (User) securityContext.getUserPrincipal();

		if (this.userStorage.removeUserAvatar(authUser)) {
			authUser.setAvatar(false);
			this.userService.update(authUser);
			return Response.status(204).build();
		} else {
			throw new EntityValidationException(
					new ApplicationError("userAvatarRemove", "The avatar of the user could not be deleted."));
		}
	}

}