package org.jsec.api.v1.resources.account.sessions;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.jsec.api.v1.config.security.ApiSecurityRole;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.entities.user.UserSession;
import org.jsec.core.data.services.user.UserSessionService;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;

/**
 * This class represents the account sessions api endpoint.
 * 
 * @author alt
 *
 */
@Path("account/sessions")
@Produces(MediaType.APPLICATION_JSON)
public class AccountSessionsResource {

	@Context
	private SecurityContext securityContext;

	private UserSessionService tokenService;

	/**
	 * Default constructor of the class.
	 */
	public AccountSessionsResource() {
		this.tokenService = new UserSessionService();
	}

	/**
	 * Returns a single session of the current user by it's unique id.
	 * 
	 * @param sessionId
	 *            The unqiue id of the session
	 * @return The session
	 */
	@GET
	@Path("{session}")
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response getSession(@PathParam("session") int sessionId)
			throws EntityNotFoundException, EntityPersistenceException {

		User authUser = (User) securityContext.getUserPrincipal();

		UserSession token = this.tokenService.getSession(authUser, sessionId);
		return Response.status(200).entity(token).build();
	}

	/**
	 * Returns all sessions of the current user.
	 * 
	 * @return The list of sessions
	 */
	@GET
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response getSessions() throws EntityPersistenceException {

		User authUser = (User) securityContext.getUserPrincipal();

		List<UserSession> tokens = this.tokenService.getSessions(authUser);
		return Response.status(200).entity(tokens).build();
	}

	/**
	 * Deletes a single session of the current user.
	 * 
	 * @param sessionId
	 *            The unqiue id of the session
	 * @return Nothing (204) on success or an error message
	 */
	@DELETE
	@Path("{session}")
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response removeSession(@PathParam("session") int sessionId)
			throws EntityValidationException, EntityNotFoundException, EntityPersistenceException {

		User authUser = (User) securityContext.getUserPrincipal();

		UserSession token = this.tokenService.getSession(authUser, sessionId);
		this.tokenService.remove(token);
		return Response.status(204).build();
	}

	/**
	 * Deletes al sessions of the current user.
	 * 
	 * @return Nothing (204) on success or an error message
	 */
	@DELETE
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response removeSessions()
			throws EntityValidationException, EntityNotFoundException, EntityPersistenceException {

		User authUser = (User) securityContext.getUserPrincipal();

		this.tokenService.removeAll(authUser);
		return Response.status(204).build();
	}

}