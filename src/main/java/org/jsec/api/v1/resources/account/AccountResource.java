package org.jsec.api.v1.resources.account;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.jsec.api.v1.config.security.ApiSecurityRole;
import org.jsec.core.data.entities.user.Account;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.services.user.UserService;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.util.error.ApplicationError;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This class represents the account api endpoint.
 * 
 * @author alt
 *
 */
@Path("account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

	@Context
	private SecurityContext securityContext;

	private UserService userService;

	/**
	 * Default constructor of the class.
	 */
	public AccountResource() {
		this.userService = new UserService();
	}

	/**
	 * Returns the user account of the currently logged in user.
	 * 
	 * @return The user account
	 */
	@GET
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response getAccount() {

		User authUser = (User) securityContext.getUserPrincipal();
		return Response.status(200).entity(new Account(authUser)).build();
	}

	/**
	 * Updates the current users attributes.
	 * 
	 * @param json
	 *            The attributes to update
	 * @return The updated user account
	 */
	@PUT
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response update(JsonNode json)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		User authUser = (User) securityContext.getUserPrincipal();

		this.userService.updateEntity(authUser, json);
		this.userService.update(authUser);
		return Response.status(200).entity(new Account(authUser)).build();
	}

	/**
	 * Updates the current users password.
	 * 
	 * @param json
	 *            The json parameters
	 * @return Success or an error message
	 */
	@PUT
	@Path("password")
	@ApiSecurityRole(ApiSecurityRole.AUTHENTICATED)
	public Response changePassword(JsonNode json) throws EntityValidationException, EntityPersistenceException,
			NoSuchAlgorithmException, InvalidKeySpecException {

		User authUser = (User) securityContext.getUserPrincipal();
		String password = null;

		if (json.has("password") && !json.get("password").isNull()) {
			password = json.get("password").asText();
		}

		if (authUser.getType() == 1) {
			this.userService.setPassword(authUser, password);
			return Response.status(200).build();
		} else {
			throw new EntityValidationException(
					new ApplicationError("invalidUserType", "The password of ldap users cannot be changed."));
		}
	}

}