package org.jsec.application;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.rewrite.handler.HeaderPatternRule;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.SecuredRedirectHandler;
import org.eclipse.jetty.server.handler.ShutdownHandler;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jsec.api.v1.config.ApiConfiguration;
import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.configuration.server.ServerConfiguration;
import org.jsec.core.exceptions.ConfigurationValidationException;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.security.cert.CertStoreUtil;

/**
 * The Jetty server management class of the application.
 * 
 * @author alt
 *
 */
public class Application {

	private static Logger log = LogManager.getLogger(Application.class);

	private ServerConfiguration configuration;
	private Server server;

	/**
	 * The startup method for the application.
	 * 
	 * @param args
	 *            The commandline arguments
	 * @throws ConfigurationValidationException
	 *             Will be thrown if an configuration error occurred
	 */
	public static void main(String[] args) throws ConfigurationValidationException {

		if (args.length > 0) {

			init();

			String command = args[0];
			switch (command) {
				case "setup":
					new ApplicationInstaller().startSetup();
					break;
				case "migrate":
					new Application().migrate();
					break;
				case "start":
					new Application().start();
					break;
				case "stop":
					new Application().stop();
					break;
				default:
					break;
			}
		}
	}

	/**
	 * Init the system variables with defaults.
	 */
	private static void init() {

		File appHome = null;

		// Check if the application.home is set. If not use the current directory
		if (System.getProperty("application.home") != null) {
			appHome = new File(System.getProperty("application.home"));
			System.setProperty("user.dir", appHome.getAbsoluteFile().getAbsolutePath());
		} else if (System.getenv("CONNECTEDCOOKING_HOME") != null) {
			appHome = new File(System.getenv("application.home"));
			System.setProperty("user.dir", appHome.getAbsoluteFile().getAbsolutePath());
		}
	}

	/**
	 * The default constructor of the class.
	 * 
	 * @throws ConfigurationValidationException
	 */
	public Application() throws ConfigurationValidationException {

		try {
			ConfigurationContext.getInstance().loadConfiguration();
			this.configuration = ConfigurationContext.getInstance().getServerConfiguration();
		} catch (ConfigurationValidationException e) {
			log.error("configurationError", e);
			throw e;
		}
	}

	/**
	 * Migrates the application database.
	 */
	public void migrate() {

		PersistenceProvider.getInstance().connect();
		PersistenceProvider.getInstance().migrate();
		PersistenceProvider.getInstance().shutdown();
	}

	/**
	 * Checks if cryptographic restrictions are in place. (only for java 7 and 8).
	 * 
	 * @return True if restricted version
	 */
	private boolean isRestrictedCryptography() {

		// This matches Oracle Java 7 and 8, but not Java 9 or OpenJDK.
		final String name = System.getProperty("java.runtime.name");
		final String ver = System.getProperty("java.version");

		return name != null && name.equals("Java(TM) SE Runtime Environment") && ver != null
				&& (ver.startsWith("1.7") || ver.startsWith("1.8"));
	}

	/**
	 * Removes all cryptographic restrictions that are in place.
	 */
	private void removeCryptographicRestrictions() {

		if (!isRestrictedCryptography()) {
			return;
		}

		try {

			/*
			 * Do the following, but with reflection to bypass access checks:
			 *
			 * JceSecurity.isRestricted = false; JceSecurity.defaultPolicy.perms.clear();
			 * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
			 */
			final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
			final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
			final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

			final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
			isRestrictedField.setAccessible(true);

			final Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(isRestrictedField, isRestrictedField.getModifiers() & ~Modifier.FINAL);
			isRestrictedField.set(null, false);

			final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
			defaultPolicyField.setAccessible(true);
			final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

			final Field perms = cryptoPermissions.getDeclaredField("perms");
			perms.setAccessible(true);
			((Map<?, ?>) perms.get(defaultPolicy)).clear();

			final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
			instance.setAccessible(true);
			defaultPolicy.add((Permission) instance.get(null));
		} catch (Exception e) {
			log.error("Application.removeCryptographyRestrictions()", e);
		}
	}

	/**
	 * Start the application server.
	 */
	public void start() {

		try {

			// Remove cryptographic restrictions
			removeCryptographicRestrictions();

			// Override java default truststore settings
			new CertStoreUtil().reloadTrustStore();

			// Check if database connection is established
			PersistenceProvider.getInstance().connect();
			if (!PersistenceProvider.getInstance().isConnected()) {
				throw new ConfigurationValidationException(
						"Error while starting application, no database connection ...");
			}

			// Load application level settings
			ConfigurationContext.getInstance().loadApplicationSettings();

			// Configuring server thread pool
			QueuedThreadPool threadPool = new QueuedThreadPool();
			threadPool.setMinThreads(configuration.getMinThreads());
			threadPool.setMaxThreads(configuration.getMaxThreads());

			// Configuring server instance
			server = new Server(threadPool);
			server.setDumpAfterStart(false);
			server.setDumpBeforeStop(false);
			server.setStopAtShutdown(true);
			server.setStopTimeout(5000);

			// Add the web application and REST API
			HandlerCollection handlers = new HandlerCollection();

			// Set the HTTP and HTTPS connectors
			createConnector(server);
			createSecureConnector(server);

			// Create the application
			createShutdownHandler(handlers);
			createSecurityHeaders(handlers);
			createApplicationAPI(handlers);
			createApplication(handlers);

			server.setHandler(handlers);

			// Start the server
			server.start();
			server.join();

		} catch (Exception e) {
			log.error("startupError", e);
			try {
				if (server != null) {
					server.stop();
					server.destroy();
				}
				PersistenceProvider.getInstance().shutdown();
			} catch (Exception e1) {
				log.error(e1);
			}
		}
	}

	/**
	 * Stops the application server.
	 */
	public void stop() {

		try {

			System.setProperty("javax.net.ssl.trustStore", configuration.getSsl().getKeyStore().getAbsolutePath());
			System.setProperty("javax.net.ssl.trustStorePassword", configuration.getSsl().getKeyStorePass());
			System.setProperty("javax.net.ssl.trustStoreType", "JKS");

			URL url = new URL("http://localhost:" + this.configuration.getPort() + "/shutdown?token=test1234");

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.getResponseCode();

		} catch (Exception e) {
			log.error("shutdownError", e);
		}
	}

	/**
	 * Creates a new non secure HTTP connector for the server instance.
	 * 
	 * @param server
	 *            The server
	 */
	private void createConnector(Server server) {

		HttpConfiguration config = new HttpConfiguration();
		config.setSendServerVersion(false);
		config.setSendXPoweredBy(false);
		config.setSendDateHeader(false);
		config.setSecureScheme("https");
		config.setSecurePort(configuration.getPortSecure());

		ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(config));
		connector.setPort(configuration.getPort());

		server.addConnector(connector);
	}

	/**
	 * Creates a new secure HTTPS connector for the server instance
	 * 
	 * @param server
	 *            The server
	 */
	private void createSecureConnector(Server server) {

		if (configuration.getSsl().isEnabled()) {
			HttpConfiguration config = new HttpConfiguration();
			config.setSendServerVersion(false);
			config.setSendXPoweredBy(false);
			config.setSendDateHeader(false);
			config.setSecureScheme("https");
			config.setSecurePort(configuration.getPortSecure());
			config.addCustomizer(new SecureRequestCustomizer());

			SslContextFactory sslContextFactory = new SslContextFactory();
			sslContextFactory.setRenegotiationAllowed(false);
			sslContextFactory.setUseCipherSuitesOrder(true);

			sslContextFactory.setKeyStorePath(configuration.getSsl().getKeyStore().getAbsolutePath());
			sslContextFactory.setKeyStorePassword(configuration.getSsl().getKeyStorePass());

			// Set DH key size parameter
			System.setProperty("jdk.tls.ephemeralDHKeySize", String.valueOf(configuration.getSsl().getDhKeySize()));

			// Set enabled elliptic curves
			System.setProperty("jdk.tls.namedGroups",
					String.join(",", configuration.getSsl().getEnabledEllipticCurves()));

			// Set enabled ssl protocols
			if (configuration.getSsl().getDisabledProtocols() != null) {
				sslContextFactory.setExcludeProtocols(configuration.getSsl().getDisabledProtocols());
			}
			if (configuration.getSsl().getEnabledProtocols() != null) {
				sslContextFactory.setIncludeProtocols(configuration.getSsl().getEnabledProtocols());
			}

			// Set enabled ssl ciphers
			if (configuration.getSsl().getEnabledCiphers() != null) {
				sslContextFactory.setIncludeCipherSuites(configuration.getSsl().getEnabledCiphers());
			}
			if (configuration.getSsl().getDisabledCiphers() != null) {
				sslContextFactory.setExcludeCipherSuites(configuration.getSsl().getDisabledCiphers());
			}

			SslConnectionFactory sslFactory = new SslConnectionFactory(sslContextFactory,
					HttpVersion.HTTP_1_1.asString());

			ServerConnector connector = new ServerConnector(server, sslFactory, new HttpConnectionFactory(config));
			connector.setPort(configuration.getPortSecure());

			server.addConnector(connector);
		}
	}

	/**
	 * Creates a new shutdown handler.
	 * 
	 * @param handlers
	 *            The handler collection
	 */
	private void createShutdownHandler(HandlerCollection handlers) {
		handlers.addHandler(new ShutdownHandler("test1234", true, true));
	}

	/**
	 * Adds security headers rules.
	 * 
	 * @param handlers
	 *            The handler collection
	 */
	private void createSecurityHeaders(HandlerCollection handlers) {

		RewriteHandler rewrite = new RewriteHandler();
		rewrite.setRewriteRequestURI(true);
		rewrite.setRewritePathInfo(false);

		HeaderPatternRule referrerPolicy = new HeaderPatternRule();
		referrerPolicy.setPattern("*");
		referrerPolicy.setName("Referrer-Policy");
		referrerPolicy.setValue("no-referrer");
		rewrite.addRule(referrerPolicy);

		HeaderPatternRule xssProtection = new HeaderPatternRule();
		xssProtection.setPattern("*");
		xssProtection.setName("X-XSS-Protection");
		xssProtection.setValue("1; mode=block");
		rewrite.addRule(xssProtection);

		HeaderPatternRule contentTypeOptions = new HeaderPatternRule();
		contentTypeOptions.setPattern("*");
		contentTypeOptions.setName("X-Content-Type-Options");
		contentTypeOptions.setValue("nosniff");
		rewrite.addRule(contentTypeOptions);

		HeaderPatternRule frameOptions = new HeaderPatternRule();
		frameOptions.setPattern("*");
		frameOptions.setName("X-Frame-Options");
		frameOptions.setValue("DENY");
		rewrite.addRule(frameOptions);

		if (configuration.getSsl().isEnabled()) {

			HeaderPatternRule transportSecurity = new HeaderPatternRule();
			transportSecurity.setPattern("*");
			transportSecurity.setName("Strict-Transport-Security");
			transportSecurity.setValue("max-age=31536000; includeSubDomains");
			rewrite.addRule(transportSecurity);

			handlers.addHandler(new SecuredRedirectHandler());
		}

		handlers.addHandler(rewrite);
	}

	/**
	 * Creates the web rest api application context.
	 * 
	 * @return The rest api application
	 */
	private void createApplicationAPI(HandlerCollection handlers) {

		ServletHolder servlet = new ServletHolder(new ServletContainer(new ApiConfiguration()));

		ServletContextHandler context = new ServletContextHandler(server, "/api/v1", ServletContextHandler.SESSIONS);
		context.addServlet(servlet, "/*");
		context.insertHandler(createRequestLog("api"));

		handlers.addHandler(context);
	}

	/**
	 * Creates the web application context.
	 * 
	 * @return The web application
	 */
	private void createApplication(HandlerCollection handlers) {

		WebAppContext context = new WebAppContext();
		context.setContextPath("/");
		context.setResourceBase(new File(ConfigurationContext.getInstance().getAppHome(), "/webapp").getAbsolutePath());
		context.setWelcomeFiles(new String[] { "index.html" });
		context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
		context.setInitParameter("org.eclipse.jetty.servlet.Default.useFileMappedBuffer", "false");
		context.setParentLoaderPriority(true);
		context.setDisplayName("ConnectedCookingServer");
		context.setTempDirectory(new File(ConfigurationContext.getInstance().getAppHome().getAbsoluteFile(), "/temp"));
		context.insertHandler(createRequestLog("web"));

		GzipHandler gzipHandler = new GzipHandler();
		gzipHandler.addIncludedMimeTypes("text/plain", "text/html", "text/css", "application/javascript",
				"application/json");
		context.insertHandler(gzipHandler);

		handlers.addHandler(context);
	}

	/**
	 * Creates a new request log handler with the specified name.
	 * 
	 * @param name
	 *            The application name
	 * @return The new request log handler
	 */
	private RequestLogHandler createRequestLog(String name) {

		File requestLogFile = new File(ConfigurationContext.getInstance().getAppHome(),
				"/logs/" + name + "_yyyy_MM_dd.log");

		NCSARequestLog requestLog = new NCSARequestLog();
		requestLog.setFilename(requestLogFile.getAbsolutePath());
		requestLog.setFilenameDateFormat("yyyy_MM_dd");
		requestLog.setRetainDays(90);
		requestLog.setAppend(true);
		requestLog.setExtended(true);
		requestLog.setLogCookies(false);
		requestLog.setLogTimeZone("GMT");

		RequestLogHandler requestLogHandler = new RequestLogHandler();
		requestLogHandler.setRequestLog(requestLog);

		return requestLogHandler;
	}

}