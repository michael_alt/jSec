package org.jsec.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.services.role.RoleService;
import org.jsec.core.data.services.settings.SettingsService;
import org.jsec.core.data.services.user.UserService;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.security.cert.CertStoreUtil;
import org.jsec.core.util.StringEncodingUtils;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

/**
 * This class is used to create an initial configuration file for the
 * application and install all required tables and settings to opperate the
 * server.
 * 
 * @author alt
 *
 */
public class ApplicationInstaller {

	private static Logger log = LogManager.getLogger(ApplicationInstaller.class);

	private Properties arguments;

	/**
	 * The default constructor.
	 */
	public ApplicationInstaller() {
		this.arguments = new Properties();
	}

	/**
	 * Prints the initial setup header and waits for confirmation.
	 */
	public void startSetup() {

		InputStreamReader streamReader = null;
		BufferedReader reader = null;

		try {

			streamReader = new InputStreamReader(System.in);
			reader = new BufferedReader(streamReader);

			System.out.println("");
			System.out.println(" Welcome to the ConnectedCookingServer installation Wizard");
			System.out.println(" =========================================================");
			System.out.println("");
			System.out.println(" This wizard will guide you throug the installation process.");
			System.out.println("");
			System.out.println(" ------ IMPORTANT ------");
			System.out.println("");
			System.out.println(" * Before installing the product, be sure to back up your data!");
			System.out.println(" * The generated SSL certificate is only temporary, it should be replaced asap!");
			System.out.println(" * This installer will remove all data!");
			System.out.println("");

			System.out.print(" Continue with the Setup [yes/no]: ");
			String startSetup = reader.readLine();

			if (!startSetup.equalsIgnoreCase("yes")) {
				System.out.println("  - Setup was aborted!");
				System.exit(0);
			}

			setupCertificate();
			setupDatabase(reader);
			saveConfigFile();

			ConfigurationContext.getInstance().loadConfiguration();
			PersistenceProvider.getInstance().connect();

			setupDefaults();
			setupAdministrator();

			PersistenceProvider.getInstance().shutdown();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("setupError", e);
		}
	}

	/**
	 * Creates a new temp SSL certificate.
	 */
	private void setupCertificate() {

		System.out.println("");
		System.out.println("");
		System.out.println(" CREATING TEMP SSL CERTIFICATE");
		System.out.println(" =========================================================");
		System.out.println("");
		System.out.println("  To increase security, the server will no create a temporary");
		System.out.println("  SSL certificate to use for the server. Please replace this");
		System.out.println("  with a valid one as soon as possible! This certificate will");
		System.out.println("  only be valid for 90 days.");
		System.out.println("");
		System.out.print("  Creating server certificate: ");

		String keyStorePassword = StringEncodingUtils.generateRandomString(20);
		String trustStorePassword = StringEncodingUtils.generateRandomString(20);
		CertStoreUtil certStoreUtil = new CertStoreUtil(null);
		if (!certStoreUtil.initKeyStore(keyStorePassword, 4096)) {
			System.out.print("error");
			System.exit(1);
		}
		if (!certStoreUtil.initTrustStore(trustStorePassword)) {
			System.out.print("error");
			System.exit(1);
		}
		System.out.print("success");

		this.arguments.put("keyStorePassword", keyStorePassword);
		this.arguments.put("trustStorePassword", trustStorePassword);
	}

	/**
	 * Reads the database connection settings from the console and tries to
	 * establish an connection to the database server.
	 * 
	 * @param reader
	 *            The console input reader
	 * @throws IOException
	 *             Will be thrown if an read error occurs
	 */
	private void setupDatabase(BufferedReader reader) throws IOException {

		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println(" DATABASE SETUP");
		System.out.println(" =========================================================");
		System.out.println("");
		System.out.println("  The server will now connect to the database serve and");
		System.out.println("  create all necessary database tables. Please provide");
		System.out.println("  the required database connection parameters.");
		System.out.println("");

		this.arguments.setProperty("applicationHttpPort", "80");
		this.arguments.setProperty("applicationHttpsPort", "443");

		String hostname = null;
		while (hostname == null || hostname.trim().equalsIgnoreCase("")) {
			System.out.print("  Database server   : ");
			hostname = reader.readLine();
		}
		this.arguments.put("databaseHostname", hostname);

		String port = null;
		while (port == null || port.trim().equalsIgnoreCase("")) {
			System.out.print("  Database port     : ");
			port = reader.readLine();
		}
		this.arguments.put("databasePort", port);

		String name = null;
		while (name == null || name.trim().equalsIgnoreCase("")) {
			System.out.print("  Database name     : ");
			name = reader.readLine();
		}
		this.arguments.put("databaseName", name);

		String username = null;
		while (username == null || username.trim().equalsIgnoreCase("")) {
			System.out.print("  Database username : ");
			username = reader.readLine();
		}
		this.arguments.put("databaseUsername", username);

		String password = null;
		while (password == null || password.trim().equalsIgnoreCase("")) {
			System.out.print("  Database password : ");
			password = reader.readLine();
		}
		this.arguments.put("databasePassword", password);

		System.out.println("");
		System.out.print("  Checking database connection : ");
		if (!PersistenceProvider.getInstance().healthCheck(this.arguments)) {
			System.out.print("error");
			System.exit(1);
		}
		System.out.print("success");
	}

	/**
	 * Installs all database migrations and populates the settings table with
	 * default values. Also creates the root account with an random password.
	 * 
	 * @throws Exception
	 *             Will be thrown if an error occurs
	 */
	private void setupDefaults() throws Exception {

		SettingsService settingsService = new SettingsService();

		System.out.println("");
		System.out.print("  Creating database tables     : ");
		PersistenceProvider.getInstance().install();
		System.out.print("success");

		System.out.println("");
		System.out.print("  Creating default settings    : ");
		settingsService.create();
		System.out.print("success");
	}

	/**
	 * Creates the server administrator.
	 * 
	 * @throws Exception
	 *             Will be thrown if an error occurs
	 */
	private void setupAdministrator() throws Exception {

		UserService userService = new UserService();
		RoleService roleServier = new RoleService();

		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println(" CREATING ROOT USER");
		System.out.println(" =========================================================");

		String rootPassword = StringEncodingUtils.generateRandomString(10);
		if (rootPassword == null) {
			StringEncodingUtils.generateRandomString(15);
		}

		User user = new User();
		user.setType(1);
		user.setUsername("root");
		user.setDisplayName("Administrator");
		user.setMail("root@localhost.de");
		userService.create(user, rootPassword);

		Role role = roleServier.getRole("ADMINISTRATOR");
		userService.addRole(user, role);

		System.out.println("");
		System.out.println("  Setup was completed successfully! Please use the start");
		System.out.println("  script to start the Service.");

		System.out.println("");
		System.out.println("  Username : root");
		System.out.println("  Password : " + rootPassword);
	}

	/**
	 * Saves the created configuration file in the conf folder.
	 * 
	 * @throws IOException
	 *             Will be thrown if an error occurs
	 */
	private void saveConfigFile() throws IOException {

		URL url = Resources.getResource("conf/server.yml.example");
		String content = Resources.toString(url, Charsets.UTF_8);
		content = content.replace("$(keyStorePassword)$", this.arguments.getProperty("keyStorePassword"));
		content = content.replace("$(trustStorePassword)$", this.arguments.getProperty("trustStorePassword"));
		content = content.replace("$(applicationHttpPort)$", this.arguments.getProperty("applicationHttpPort"));
		content = content.replace("$(applicationHttpsPort)$", this.arguments.getProperty("applicationHttpsPort"));
		content = content.replace("$(databaseHostname)$", this.arguments.getProperty("databaseHostname"));
		content = content.replace("$(databasePort)$", this.arguments.getProperty("databasePort"));
		content = content.replace("$(databaseName)$", this.arguments.getProperty("databaseName"));
		content = content.replace("$(databaseUsername)$", this.arguments.getProperty("databaseUsername"));
		content = content.replace("$(databasePassword)$", this.arguments.getProperty("databasePassword"));

		Files.write(Paths.get(System.getProperty("user.dir"), "conf/server.yml"), content.getBytes(),
				StandardOpenOption.CREATE);
	}

}