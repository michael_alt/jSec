package org.jsec.core.storage.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.naming.SizeLimitExceededException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.storage.StorageProvider;
import org.jsec.core.util.error.ApplicationError;

/**
 * This class is used to manage file system operations for users.
 * 
 * @author alt
 *
 */
public class UserStorageProvider extends StorageProvider {

	private static final Logger log = LogManager.getLogger(UserStorageProvider.class);
	private File userDataDirectory;

	/**
	 * The default constructor.
	 */
	public UserStorageProvider() {

		this.userDataDirectory = new File(this.dataDirectory, "/webapp/uploads/users/avatars").getAbsoluteFile();
		if (!this.userDataDirectory.exists()) {
			this.userDataDirectory.mkdirs();
		}
	}

	/**
	 * Creates or updates the avatar image of the user account.
	 * 
	 * @param user
	 *            The user
	 * @param inputStream
	 *            The input stream of the file
	 * @return True if successful or false if not
	 * @throws EntityValidationException
	 *             Will be thrown if the uploaded image was to large
	 */
	public boolean storeUserAvatar(User user, InputStream inputStream) throws EntityValidationException {

		File tempFile = null;
		File userAvatar = null;

		try {

			tempFile = File.createTempFile("ccs-", "tmp");
			createTempFile(tempFile, inputStream, 204800);

			userAvatar = new File(this.userDataDirectory, user.getId() + ".png");
			Files.copy(Paths.get(tempFile.getAbsolutePath()), Paths.get(userAvatar.getAbsolutePath()),
					StandardCopyOption.REPLACE_EXISTING);

			return true;
		} catch (FileNotFoundException e) {
			log.error("UserStorageProvider.storeUserAvatar()", e);
		} catch (IOException e) {
			log.error("UserStorageProvider.storeUserAvatar()", e);
		} catch (SizeLimitExceededException e) {
			throw new EntityValidationException(new ApplicationError("userAvatarToLarge", e.getMessage()));
		} finally {
			if (tempFile != null) {
				try {
					Files.deleteIfExists(Paths.get(tempFile.getAbsolutePath()));
				} catch (IOException e) {
				}
			}
		}

		return false;
	}

	/**
	 * Creates or updates the avatar image of the user account.
	 * 
	 * @param user
	 *            The user
	 * @param imageData
	 *            The image data
	 * @return True if successful or false if not
	 * @throws EntityValidationException
	 *             Will be thrown if the uploaded image was to large
	 */
	public boolean storeUserAvatar(User user, byte[] imageData) throws EntityValidationException {

		File tempFile = null;
		File userAvatar = null;

		try {

			tempFile = File.createTempFile("ccs-", "tmp");
			createTempFile(tempFile, imageData);

			userAvatar = new File(this.userDataDirectory, user.getId() + ".png");
			Files.copy(Paths.get(tempFile.getAbsolutePath()), Paths.get(userAvatar.getAbsolutePath()),
					StandardCopyOption.REPLACE_EXISTING);

			return true;
		} catch (FileNotFoundException e) {
			log.error("UserStorageProvider.storeUserAvatar()", e);
		} catch (IOException e) {
			log.error("UserStorageProvider.storeUserAvatar()", e);
		} finally {
			if (tempFile != null) {
				try {
					Files.deleteIfExists(Paths.get(tempFile.getAbsolutePath()));
				} catch (IOException e) {
				}
			}
		}

		return false;
	}

	/**
	 * Removes the avatar image of the user account.
	 * 
	 * @param user
	 *            The user
	 * @return True if successful or false if not
	 */
	public boolean removeUserAvatar(User user) {

		File userAvatar = new File(this.userDataDirectory, user.getId() + ".png");
		if (userAvatar.getAbsoluteFile().exists()) {
			return userAvatar.getAbsoluteFile().delete();
		} else {
			return true;
		}
	}

}