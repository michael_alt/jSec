package org.jsec.core.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.naming.SizeLimitExceededException;

import org.jsec.core.configuration.ConfigurationContext;

/**
 * 
 * @author alt
 *
 */
public abstract class StorageProvider {

	public final File dataDirectory = ConfigurationContext.getInstance().getAppHome().getAbsoluteFile();

	/**
	 * Creates a temporary file from the byte array.
	 * 
	 * @param tempFile
	 *            The path to the temp file
	 * @param data
	 *            The input data
	 * @throws FileNotFoundException
	 *             Will be thrown if the temp file could not be created
	 * @throws IOException
	 *             Will be thrown if an error occurred while reading the input
	 *             stream
	 */
	public final void createTempFile(File tempFile, byte[] data) throws FileNotFoundException, IOException {

		FileOutputStream outputStream = null;

		try {

			outputStream = new FileOutputStream(tempFile);
			outputStream.write(data, 0, data.length);
			outputStream.flush();

		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Creates a temporary file from the input stream.
	 * 
	 * @param tempFile
	 *            The path to the temp file
	 * @param inputStream
	 *            The input stream
	 * @throws FileNotFoundException
	 *             Will be thrown if the temp file could not be created
	 * @throws IOException
	 *             Will be thrown if an error occurred while reading the input
	 *             stream
	 * @throws SizeLimitExceededException
	 *             Will be thrown if the number of bytes read from the inputstream
	 *             was larger than the maxLength
	 */
	public final void createTempFile(File tempFile, InputStream inputStream)
			throws FileNotFoundException, IOException, SizeLimitExceededException {

		createTempFile(tempFile, inputStream, -1);
	}

	/**
	 * Creates a temporary file from the input stream.
	 * 
	 * @param tempFile
	 *            The path to the temp file
	 * @param inputStream
	 *            The input stream
	 * @param maxLength
	 *            The maximum number of bytes to read from the stream, -1 for
	 *            unlimited size
	 * @throws IOException
	 *             Will be thrown if an error occurred while reading the input
	 *             stream
	 * @throws SizeLimitExceededException
	 *             Will be thrown if the number of bytes read from the inputstream
	 *             was larger than the maxLength
	 */
	public final void createTempFile(File tempFile, InputStream inputStream, int maxLength)
			throws IOException, SizeLimitExceededException {

		FileOutputStream outputStream = null;

		try {

			if (inputStream == null) {
				throw new IOException("invalid file, empty.");
			}

			outputStream = new FileOutputStream(tempFile);

			byte[] buffer = new byte[4096];
			int read = 0;
			int total = 0;

			while ((read = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, read);
				total += read;

				if (maxLength != -1 && total >= maxLength) {
					throw new SizeLimitExceededException(String.format("File to large, exceded %s bytes", maxLength));
				}
			}

			if (total == 0) {
				throw new IOException("invalid file, empty.");
			}

			outputStream.flush();
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
			}
		}
	}

}