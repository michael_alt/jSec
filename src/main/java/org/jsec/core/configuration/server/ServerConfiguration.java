package org.jsec.core.configuration.server;

import java.io.File;

import org.jsec.core.exceptions.ConfigurationValidationException;

/**
 * This class represents the webserver configuration.
 * 
 * @author alt
 *
 */
public class ServerConfiguration {

	private int port;
	private int portSecure;

	private File trustStore;
	private String trustStorePass;

	private int minThreads;
	private int maxThreads;

	private ServerLoggingConfiguration logging;
	private ServerSSLConfiguration ssl;

	public ServerConfiguration() {
		// Default constructor
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPortSecure() {
		return portSecure;
	}

	public void setPortSecure(int portSecure) {
		this.portSecure = portSecure;
	}

	public File getTrustStore() {
		return trustStore;
	}

	public void setTrustStore(File trustStore) {
		this.trustStore = trustStore;
	}

	public String getTrustStorePass() {
		return trustStorePass;
	}

	public void setTrustStorePass(String trustStorePass) {
		this.trustStorePass = trustStorePass;
	}

	public int getMinThreads() {
		return minThreads;
	}

	public void setMinThreads(int minThreads) {
		this.minThreads = minThreads;
	}

	public int getMaxThreads() {
		return maxThreads;
	}

	public void setMaxThreads(int maxThreads) {
		this.maxThreads = maxThreads;
	}

	public ServerLoggingConfiguration getLogging() {
		return logging;
	}

	public void setLogging(ServerLoggingConfiguration logging) {
		this.logging = logging;
	}

	public ServerSSLConfiguration getSsl() {
		return ssl;
	}

	public void setSsl(ServerSSLConfiguration ssl) {
		this.ssl = ssl;
	}

	public void validate() throws ConfigurationValidationException {

		if (port <= 0 || port > 65535) {
			throw new ConfigurationValidationException("Please specify a valid 'port', e.g. 80 for HTTP.");
		}

		if (portSecure <= 0 || portSecure > 65535) {
			throw new ConfigurationValidationException("Please specify a valid 'portSecure', e.g. 443 for HTTPS.");
		}

		if (minThreads <= 0) {
			throw new ConfigurationValidationException("Please specify a valid 'minThreads' number of threads.");
		}

		if (maxThreads <= 0) {
			throw new ConfigurationValidationException("Please specify a valid 'maxThreads' number of threads.");
		}

		this.ssl.validate();
	}

}