package org.jsec.core.configuration.server;

import java.io.File;

import org.jsec.core.exceptions.ConfigurationValidationException;

/**
 * This class represents the SSL configuration of the webserver.
 * 
 * @author alt
 *
 */
public class ServerSSLConfiguration {

	private boolean enabled;
	private File keyStore;
	private String keyStorePass;
	private int dhKeySize;

	private String[] enabledProtocols;
	private String[] disabledProtocols;
	private String[] enabledCiphers;
	private String[] disabledCiphers;
	private String[] enabledEllipticCurves;

	public ServerSSLConfiguration() {
		// Default constructor
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public File getKeyStore() {
		return keyStore;
	}

	public void setKeyStore(File keyStore) {
		this.keyStore = keyStore;
	}

	public String getKeyStorePass() {
		return keyStorePass;
	}

	public void setKeyStorePass(String keyStorePass) {
		this.keyStorePass = keyStorePass;
	}

	public int getDhKeySize() {
		return dhKeySize;
	}

	public void setDhKeySize(int dhKeySize) {
		this.dhKeySize = dhKeySize;
	}

	public String[] getEnabledProtocols() {
		return enabledProtocols;
	}

	public void setEnabledProtocols(String[] enabledProtocols) {
		this.enabledProtocols = enabledProtocols;
	}

	public String[] getDisabledProtocols() {
		return disabledProtocols;
	}

	public void setDisabledProtocols(String[] disabledProtocols) {
		this.disabledProtocols = disabledProtocols;
	}

	public String[] getEnabledCiphers() {
		return enabledCiphers;
	}

	public void setEnabledCiphers(String[] enabledCiphers) {
		this.enabledCiphers = enabledCiphers;
	}

	public String[] getDisabledCiphers() {
		return disabledCiphers;
	}

	public void setDisabledCiphers(String[] disabledCiphers) {
		this.disabledCiphers = disabledCiphers;
	}

	public String[] getEnabledEllipticCurves() {
		return enabledEllipticCurves;
	}

	public void setEnabledEllipticCurves(String[] enabledEllipticCurves) {
		this.enabledEllipticCurves = enabledEllipticCurves;
	}

	public void validate() throws ConfigurationValidationException {

		if (this.isEnabled()) {

			if (this.keyStore == null || !this.keyStore.getAbsoluteFile().exists()) {
				throw new ConfigurationValidationException(
						"Please specify a valid 'keyStore', file could not be found");
			}

			if (this.keyStorePass == null || this.keyStorePass.equalsIgnoreCase("")) {
				throw new ConfigurationValidationException("Please specify a valid 'keyStorePass' passphrase.");
			}

			if (this.dhKeySize < 1024) {
				throw new ConfigurationValidationException(
						"Please specify a 'dhKeySize' larger than 1024. Any lower value is deemed insecure.");
			}
		}
	}

}