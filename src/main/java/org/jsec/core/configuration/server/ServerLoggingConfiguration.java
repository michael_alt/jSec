package org.jsec.core.configuration.server;

/**
 * This class represents the logging configuration of the webserver.
 * 
 * @author alt
 *
 */
public class ServerLoggingConfiguration {

	private String level;
	private int maxAge;

	public ServerLoggingConfiguration() {
		// Default constructor
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

}