package org.jsec.core.configuration;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.jsec.core.configuration.database.DatabaseConfiguration;
import org.jsec.core.configuration.server.ServerConfiguration;
import org.jsec.core.data.entities.settings.Settings;
import org.jsec.core.data.services.settings.SettingsService;
import org.jsec.core.exceptions.ConfigurationValidationException;
import org.jsec.core.exceptions.EntityPersistenceException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * This class is used to load the server configuration.
 * 
 * @author alt
 *
 */
public class ConfigurationContext {

	private Configuration configuration;
	private Settings settings;

	private File appHome;

	/**
	 * The ConfigurationContextFactory.
	 */
	public static final class ConfigurationContextFactory {
		static final ConfigurationContext INSTANCE = new ConfigurationContext();

		private ConfigurationContextFactory() {
		}
	}

	/**
	 * Returns the default instance.
	 * 
	 * @return The ConfigurationContext instance
	 */
	public static synchronized ConfigurationContext getInstance() {
		return ConfigurationContextFactory.INSTANCE;
	}

	/**
	 * The default constructor of the class.
	 */
	private ConfigurationContext() {
	}

	/**
	 * Loads all settings from the "conf/server.yml" file.
	 * 
	 * @throws ConfigurationValidationException
	 *             Will be thrown if an error occurs
	 */
	public void loadConfiguration() throws ConfigurationValidationException {

		try {

			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			this.appHome = new File(System.getProperty("user.dir"));

			// Create temp dir
			File tempFile = new File(this.appHome, "/temp");
			if (!tempFile.getAbsoluteFile().exists()) {
				tempFile.mkdirs();
			}

			// Set temp dir
			System.setProperty("java.io.tmpdir", tempFile.getAbsolutePath());

			// Set the application configuration file directory
			File serverConfig = new File(this.appHome.getAbsoluteFile(), "conf/server.yml");
			if (serverConfig.exists()) {
				this.configuration = mapper.readValue(serverConfig, Configuration.class);
				this.configuration.getServer().validate();
				this.configuration.getDatabase().validate();
			} else {
				throw new IOException("configuration file: server.yml not found");
			}

			// Set logging configuration
			System.setProperty("application.home", this.appHome.getAbsoluteFile().getAbsolutePath());
			System.setProperty("application.log.level", this.configuration.getServer().getLogging().getLevel());
			System.setProperty("application.log.maxAge",
					String.valueOf(this.configuration.getServer().getLogging().getMaxAge()));

			LoggerContext context = (LoggerContext) LogManager.getContext(false);
			context.reconfigure();

		} catch (IOException e) {
			throw new ConfigurationValidationException(e);
		}
	}

	/**
	 * Loads all application settings from the database.
	 * 
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public void loadApplicationSettings() throws EntityPersistenceException {

		if (this.settings == null) {
			this.settings = new SettingsService().getSettings();
		} else {
			new SettingsService().reload(settings);
		}
	}

	/**
	 * Returns the server configuration of the application.
	 * 
	 * @return The server configuration
	 */
	public ServerConfiguration getServerConfiguration() {
		return this.configuration.getServer();
	}

	/**
	 * Returns the database configuration of the application.
	 * 
	 * @return The database configuration
	 */
	public DatabaseConfiguration getDatabaseConfiguration() {
		return this.configuration.getDatabase();
	}

	/**
	 * Returns the application settings.
	 * 
	 * @return The application settings
	 */
	public Settings getApplicationSettings() {
		return this.settings;
	}

	/**
	 * Returns the home directory of the application.
	 * 
	 * @return The directory
	 */
	public File getAppHome() {
		return this.appHome;
	}

}