package org.jsec.core.configuration;

import org.jsec.core.configuration.database.DatabaseConfiguration;
import org.jsec.core.configuration.server.ServerConfiguration;

/**
 * This class represents the application configuration.
 * 
 * @author alt
 *
 */
public class Configuration {

	private ServerConfiguration server;
	private DatabaseConfiguration database;

	public Configuration() {
		// Default constructor
	}

	public ServerConfiguration getServer() {
		return server;
	}

	public void setServer(ServerConfiguration server) {
		this.server = server;
	}

	public DatabaseConfiguration getDatabase() {
		return database;
	}

	public void setDatabase(DatabaseConfiguration database) {
		this.database = database;
	}

}