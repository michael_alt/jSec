package org.jsec.core.configuration.database;

import org.jsec.core.exceptions.ConfigurationValidationException;

/**
 * This class specifies the configuration for the database connector.
 * 
 * @author alt
 *
 */
public class DatabaseConfiguration {

	private String hostname;
	private int port;
	private String database;
	private String username;
	private String password;

	private DatabaseSSLConfiguration ssl;
	private DatabaseConnectionConfiguration connection;

	public DatabaseConfiguration() {
		this.ssl = new DatabaseSSLConfiguration();
		this.connection = new DatabaseConnectionConfiguration();
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public DatabaseSSLConfiguration getSsl() {
		return ssl;
	}

	public void setSsl(DatabaseSSLConfiguration ssl) {
		this.ssl = ssl;
	}

	public DatabaseConnectionConfiguration getConnections() {
		return connection;
	}

	public void setConnections(DatabaseConnectionConfiguration connection) {
		this.connection = connection;
	}

	public void validate() throws ConfigurationValidationException {

		if (hostname == null || hostname.equalsIgnoreCase("")) {
			throw new ConfigurationValidationException("Please specify a valid database hostname.");
		}

		if (port <= 0 || port >= 65535) {
			throw new ConfigurationValidationException("Please specify a valid database port, e.g. 3306 for mysql.");
		}

		if (database == null || database.equalsIgnoreCase("")) {
			throw new ConfigurationValidationException("Please specify a valid database name.");
		}

		if (username == null || username.equalsIgnoreCase("")) {
			throw new ConfigurationValidationException("Please specify a valid database username.");
		}

		if (password == null || password.equalsIgnoreCase("")) {
			throw new ConfigurationValidationException("Please specify a valid database password.");
		}

		this.ssl.validate();
		this.connection.validate();
	}

}