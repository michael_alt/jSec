package org.jsec.core.configuration.database;

import org.jsec.core.exceptions.ConfigurationValidationException;

/**
 * This class specifies the connection pool configuration of the database
 * connector.
 * 
 * @author alt
 *
 */
public class DatabaseConnectionConfiguration {

	private int min;
	private int max;
	private int maxCached;
	private int timeout;
	private int timeoutValidation;

	public DatabaseConnectionConfiguration() {
		// Default constructor
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMaxCached() {
		return maxCached;
	}

	public void setMaxCached(int maxCached) {
		this.maxCached = maxCached;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getTimeoutValidation() {
		return timeoutValidation;
	}

	public void setTimeoutValidation(int timeoutValidation) {
		this.timeoutValidation = timeoutValidation;
	}

	public void validate() throws ConfigurationValidationException {

		if (this.getMin() <= 0) {
			throw new ConfigurationValidationException("Please specify a valid database min connection pool.");
		}

		if (this.getMax() <= 0) {
			throw new ConfigurationValidationException("Please specify a valid database max connection pool.");
		}

		if (this.getMaxCached() <= 0) {
			throw new ConfigurationValidationException("Please specify a valid database max cached connections.");
		}

		if (this.getTimeout() <= 0) {
			throw new ConfigurationValidationException("Please specify a valid database connection timeout.");
		}

		if (this.getTimeoutValidation() <= 0) {
			throw new ConfigurationValidationException(
					"Please specify a valid database connection validation timeout.");
		}
	}

}