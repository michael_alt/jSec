package org.jsec.core.configuration.database;

import java.io.File;

import org.jsec.core.exceptions.ConfigurationValidationException;

/**
 * This class specifies SSL configuration for the database connector.
 * 
 * @author alt
 *
 */
public class DatabaseSSLConfiguration {

	private boolean enabled;
	private boolean verifyCertificate;
	private File clientCertKeystore;
	private String clientCertKeystorePass;
	private String clientCertKeystoreType;

	public DatabaseSSLConfiguration() {
		// Default constructor
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isVerifyCertificate() {
		return verifyCertificate;
	}

	public void setVerifyCertificate(boolean verifyCertificate) {
		this.verifyCertificate = verifyCertificate;
	}

	public File getClientCertKeystore() {
		return clientCertKeystore;
	}

	public void setClientCertKeystore(File clientCertKeystore) {
		this.clientCertKeystore = clientCertKeystore;
	}

	public String getClientCertKeystorePass() {
		return clientCertKeystorePass;
	}

	public void setClientCertKeystorePass(String clientCertKeystorePass) {
		this.clientCertKeystorePass = clientCertKeystorePass;
	}

	public String getClientCertKeystoreType() {
		return clientCertKeystoreType;
	}

	public void setClientCertKeystoreType(String clientCertKeystoreType) {
		this.clientCertKeystoreType = clientCertKeystoreType;
	}

	public void validate() throws ConfigurationValidationException {

		if (this.enabled) {

			if (this.clientCertKeystore == null || !this.clientCertKeystore.exists()) {
				throw new ConfigurationValidationException(
						"Please specify a valid 'clientCertKeystore', file could not be found");
			}

			if (this.clientCertKeystorePass == null || this.clientCertKeystorePass.equalsIgnoreCase("")) {
				throw new ConfigurationValidationException(
						"Please specify a valid 'clientCertKeystorePass', null or empty.");
			}

			if (this.clientCertKeystoreType == null || this.clientCertKeystoreType.equalsIgnoreCase("")) {
				throw new ConfigurationValidationException(
						"Please specify a valid 'clientCertKeystorePass', null or empty.");
			}
		}
	}

}