package org.jsec.core.persistence.adapter;

import java.util.Properties;

import org.hibernate.cfg.Configuration;

/**
 * This class specifies the interface that is used to set database system
 * specific settings.
 * 
 * @author alt
 *
 */
public interface DatabaseAdapter {

	/**
	 * Returns the hibernate configuration to use.
	 * 
	 * @return The hibernate configuration
	 */
	public Configuration getConfiguration();

	/**
	 * Checks if a connection to the database server could be established.
	 * 
	 * @return True if connection was possible, false if not
	 */
	public boolean healthCheck(Properties properties);

}