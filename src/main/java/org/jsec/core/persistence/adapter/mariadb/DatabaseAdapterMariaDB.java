package org.jsec.core.persistence.adapter.mariadb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.cfg.Configuration;
import org.jsec.core.configuration.database.DatabaseConfiguration;
import org.jsec.core.persistence.adapter.DatabaseAdapter;

/**
 * This class implements a MariaDB specific hibernate configuration.
 * 
 * @author alt
 *
 */
public class DatabaseAdapterMariaDB implements DatabaseAdapter {

	private static Logger log = LogManager.getLogger(DatabaseAdapter.class);

	private DatabaseConfiguration databaseConfiguration;

	public DatabaseAdapterMariaDB() {
	}

	public DatabaseAdapterMariaDB(DatabaseConfiguration databaseConfiguration) {
		this.databaseConfiguration = databaseConfiguration;
	}

	@Override
	public Configuration getConfiguration() {

		Configuration configuration = new Configuration();

		configuration.setProperty("hibernate.connection.username", databaseConfiguration.getUsername());
		configuration.setProperty("hibernate.connection.password", databaseConfiguration.getPassword());

		configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MariaDBDialect");
		configuration.setProperty("hibernate.connection.driver_class", "org.mariadb.jdbc.Driver");
		configuration.setProperty("hibernate.connection.url", getConnectionString());

		return configuration;
	}

	@Override
	public boolean healthCheck(Properties properties) {

		Connection connection = null;

		try {

			String hostname = properties.getProperty("databaseHostname");
			String port = properties.getProperty("databasePort");
			String name = properties.getProperty("databaseName");
			String username = properties.getProperty("databaseUsername");
			String password = properties.getProperty("databasePassword");

			String connectionUrl = String.format("jdbc:mariadb://%s:%s/%s", hostname, port, name);

			Class.forName("org.mariadb.jdbc.Driver");
			connection = DriverManager.getConnection(connectionUrl, username, password);
			return true;
		} catch (Exception e) {
			log.error("DatabaseAdapterMariaDB.healthCheck()", e);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				log.error("DatabaseAdapterMariaDB.healthCheck()", e);
			}
		}

		return false;
	}

	public String getConnectionString() {

		String connection = "jdbc:mariadb://{{hostname}}:{{port}}/{{database}}?";
		connection = connection.replace("{{hostname}}", databaseConfiguration.getHostname());
		connection = connection.replace("{{port}}", String.valueOf(databaseConfiguration.getPort()));
		connection = connection.replace("{{database}}", databaseConfiguration.getDatabase());
		connection = connection.concat("logger=org.mariadb.jdbc.internal.logging.Sl4JLogger");
		connection = connection.concat("&useTimezone=false");

		if (databaseConfiguration.getSsl().isEnabled()) {

			connection = connection.concat("&useSSL=true");
			connection = connection.concat("&requireSSL=true");
			connection = connection
					.concat("&verifyServerCertificate=" + databaseConfiguration.getSsl().isVerifyCertificate());

			if (databaseConfiguration.getSsl().getClientCertKeystore() != null) {

				// Set truststore parameters
				connection = connection.concat(
						"&trustCertificateKeyStoreType=" + databaseConfiguration.getSsl().getClientCertKeystoreType());
				connection = connection.concat("&trustCertificateKeyStoreUrl="
						+ databaseConfiguration.getSsl().getClientCertKeystore().toURI().toString());
				connection = connection.concat("&trustCertificateKeyStorePassword="
						+ databaseConfiguration.getSsl().getClientCertKeystorePass());

				// Set keystore parameters
				connection = connection.concat(
						"&clientCertificateKeyStoreType=" + databaseConfiguration.getSsl().getClientCertKeystoreType());
				connection = connection.concat("&clientCertificateKeyStoreUrl="
						+ databaseConfiguration.getSsl().getClientCertKeystore().toURI().toString());
				connection = connection.concat("&clientCertificateKeyStorePassword="
						+ databaseConfiguration.getSsl().getClientCertKeystorePass());
			}
		}

		return connection;
	}

}