package org.jsec.core.persistence;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.configuration.database.DatabaseConfiguration;
import org.jsec.core.persistence.adapter.DatabaseAdapter;
import org.jsec.core.persistence.adapter.mariadb.DatabaseAdapterMariaDB;
import org.reflections.Reflections;

import com.mchange.v2.c3p0.C3P0Registry;

/**
 * This class is used to access a database via an hibernate session.
 * 
 * @author alt
 *
 */
public class PersistenceProvider {

	private static final Logger log = LogManager.getLogger(PersistenceProvider.class);

	private SessionFactory sessionFactory;
	private DatabaseConfiguration databaseConfiguration;

	/**
	 * The PersistenceProviderFactory.
	 */
	private static final class PersistenceProviderFactory {
		static final PersistenceProvider INSTANCE = new PersistenceProvider();

		private PersistenceProviderFactory() {
		}
	}

	/**
	 * Returns an instance of the PersistenceProvider class.
	 * 
	 * @return The instance of the class
	 */
	public static synchronized PersistenceProvider getInstance() {
		return PersistenceProviderFactory.INSTANCE;
	}

	/**
	 * The default constructor of the class.
	 */
	private PersistenceProvider() {
		// Default constructor
	}

	/**
	 * Removes all existing tables and installs a clean database schema.
	 */
	public void install() {

		DatabaseConfiguration configuration = ConfigurationContext.getInstance().getDatabaseConfiguration();
		Flyway database = new Flyway();

		Properties properties = new Properties();
		properties.setProperty("flyway.table", "schema_version");
		database.configure(properties);

		database.setDataSource(new DatabaseAdapterMariaDB(configuration).getConnectionString(),
				configuration.getUsername(), configuration.getPassword());

		database.clean();
		database.migrate();
	}

	/**
	 * Migrates the existing database schema.
	 * 
	 * @throws FlywayException
	 *             Will be thrown if an error during the migration occurred
	 */
	public void migrate() {

		DatabaseConfiguration configuration = ConfigurationContext.getInstance().getDatabaseConfiguration();
		Flyway database = new Flyway();

		Properties properties = new Properties();
		properties.setProperty("flyway.table", "schema_version");
		database.configure(properties);

		database.setDataSource(new DatabaseAdapterMariaDB(configuration).getConnectionString(),
				configuration.getUsername(), configuration.getPassword());
		database.migrate();
	}

	/**
	 * Establish the database connection.
	 * 
	 * @throws ExceptionInInitializerError
	 *             Will be thrown if an error during the connection occurs
	 */
	public void connect() throws ExceptionInInitializerError {

		this.databaseConfiguration = ConfigurationContext.getInstance().getDatabaseConfiguration();

		DatabaseAdapter adapter = null;

		Configuration configuration = null;
		StandardServiceRegistry serviceRegistry = null;

		try {

			// DBMS specific adapter, currently only mariadb support
			adapter = new DatabaseAdapterMariaDB(databaseConfiguration);

			// Returns the adapter specific hibernate configuration
			configuration = adapter.getConfiguration();

			// Load default configuration
			loadDefaultConfiguration(configuration);

			// Load Entity Classes
			loadEntityClasses(configuration);

			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

			// builds a session factory from the service registry
			this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Exception ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Checks if the provided database connection properties allow a successful
	 * connection to the database server.
	 * 
	 * @param properties
	 *            The connection properties
	 * @return True if connection was possible, false if not
	 */
	public boolean healthCheck(Properties properties) {

		DatabaseAdapter adapter = new DatabaseAdapterMariaDB();
		return adapter.healthCheck(properties);
	}

	/**
	 * Returns the default configuration for all database adapters.
	 * 
	 * @param configuration
	 *            The hibernate configuration
	 */
	private void loadDefaultConfiguration(Configuration configuration) {

		configuration.setProperty("hibernate.current_session_context_class", "thread");
		configuration.setProperty("hibernate.connection.CharSet", "utf8");
		configuration.setProperty("hibernate.connection.useUnicode", "true");
		configuration.setProperty("hibernate.connection.provider_class",
				"org.hibernate.connection.C3P0ConnectionProvider");

		// Set connection pool settings
		configuration.setProperty("hibernate.c3p0.validate", "true");
		configuration.setProperty("hibernate.c3p0.min_size",
				String.valueOf(databaseConfiguration.getConnections().getMin()));
		configuration.setProperty("hibernate.c3p0.max_size",
				String.valueOf(databaseConfiguration.getConnections().getMax()));
		configuration.setProperty("hibernate.c3p0.max_statements",
				String.valueOf(databaseConfiguration.getConnections().getMaxCached()));
		configuration.setProperty("hibernate.c3p0.timeout",
				String.valueOf(databaseConfiguration.getConnections().getTimeout()));
		configuration.setProperty("hibernate.c3p0.idle_test_period",
				String.valueOf(databaseConfiguration.getConnections().getTimeoutValidation()));
	}

	/**
	 * Adds annotated entity classes to the hibernate configuration.
	 * 
	 * @param configuration
	 *            The hibernate configuration
	 */
	private void loadEntityClasses(Configuration configuration) {

		Reflections reflections = new Reflections("org.jsec.core.data.entities");
		Set<Class<?>> entities = reflections.getTypesAnnotatedWith(Entity.class);
		for (Class<?> entity : entities) {
			configuration.addAnnotatedClass(entity);
		}
	}

	/**
	 * Checks if the current database connection is established.
	 * 
	 * @return True if connected, False if not
	 */
	public boolean isConnected() {

		try (Connection connection = C3P0Registry.pooledDataSourceByName("jSecDatasource").getConnection()) {
			if (!connection.isClosed()) {
				return true;
			}
		} catch (Exception e) {
			log.error("PersistenceProvider.isConnected()", e);
		}

		return false;
	}

	/**
	 * Returns the cause error message of the current database connection.
	 * 
	 * @return An error message or null of the connection is established
	 */
	public String getConnectionErrorMessage() {

		try (Connection connection = C3P0Registry.pooledDataSourceByName("jSecDatasource").getConnection()) {
			if (!connection.isClosed()) {
				return "connected";
			}
		} catch (SQLException e) {
			return e.getCause().getCause().getMessage();
		}

		return null;
	}

	/**
	 * Creates a new hibernate session.
	 * 
	 * @return The new session
	 */
	public Session createSession() {
		return this.sessionFactory.openSession();
	}

	/**
	 * Safely commits and closes an open hibernate session
	 * 
	 * @param session
	 *            The hibernate session to close.
	 */
	public void close(Session session) {

		if (session != null) {
			try {
				session.close();
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	/**
	 * Creates a new entity manager instance.
	 * 
	 * @return The entity manager instance
	 */
	public EntityManager createEntityManager() {
		return this.sessionFactory.createEntityManager();
	}

	/**
	 * Safely commits and closes an entity manager instance.
	 * 
	 * @param entityManager
	 *            The EntityManager to close.
	 */
	public void close(EntityManager entityManager) {

		if (entityManager != null) {
			try {
				entityManager.close();
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	/**
	 * Closes all open hibernate sessions.
	 */
	public void shutdown() {

		if (this.sessionFactory != null) {
			try {
				this.sessionFactory.close();
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

}