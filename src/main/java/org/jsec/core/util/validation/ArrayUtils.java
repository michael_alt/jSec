package org.jsec.core.util.validation;

/**
 * This class provides helper functions for arrays.
 * 
 * @author alt
 *
 */
public class ArrayUtils {

	/**
	 * The default constructor of the class.
	 */
	private ArrayUtils() {
		// The default constructor
	}

	/**
	 * Combines the provided byte array into one array.
	 * 
	 * @param arrays
	 *            The arrays
	 * @return The new combined array
	 */
	public static byte[] concat(byte[]... arrays) {

		int length = 0;
		for (byte[] array : arrays) {
			length += array.length;
		}

		byte[] newArray = new byte[length];
		int index = 0;
		for (byte[] array : arrays) {
			System.arraycopy(array, 0, newArray, index, array.length);
			index += array.length;
		}

		return newArray;
	}

	/**
	 * Extract a sub array out of the byte array.
	 * 
	 * @param array
	 *            The array to extract from
	 * @param startIndex
	 *            The start index of the sub array
	 * @param endIndex
	 *            The end index of the sub array
	 * @return The new sub array
	 */
	public static byte[] subArray(byte[] array, int startIndex, int endIndex) {

		int length = endIndex - startIndex;
		byte[] subArray = new byte[length];

		System.arraycopy(array, startIndex, subArray, 0, length);
		return subArray;
	}

}