package org.jsec.core.util.error;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is used to provide detailed error descriptions.
 * 
 * @author alt
 *
 */
public class ApplicationError implements Serializable {

	private static final long serialVersionUID = -4284686408027810451L;

	private String code;
	private String message;

	public ApplicationError() {
	}

	public ApplicationError(String code, String message) {
		this.code = code;
		this.message = message;
	}

	@JsonProperty
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}