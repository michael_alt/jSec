package org.jsec.core.util.pagination;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a result set page container.
 * 
 * @author alt
 *
 * @param <T>
 *            The entity type
 */
public class Pagination<T> {

	private long page;
	private long pages;
	private long pageSize;
	private long entriesTotal;

	private List<T> content;

	public Pagination() {
		this.content = new LinkedList<>();
	}

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

	public long getPages() {
		return pages;
	}

	public void setPages(long pages) {
		this.pages = pages;
	}

	public long getPageSize() {
		return pageSize;
	}

	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}

	public long getEntriesTotal() {
		return entriesTotal;
	}

	public void setEntriesTotal(long entriesTotal) {
		this.entriesTotal = entriesTotal;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

}