package org.jsec.core.util;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * This class provides helper functions for encoding data.
 * 
 * @author alt
 *
 */
public class StringEncodingUtils {

	private static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()-_=+[{]}\\|;:,<.>?";
	private static final SecureRandom secureRandom = new SecureRandom();

	/**
	 * The default constructor of the class.
	 */
	private StringEncodingUtils() {
	}

	/**
	 * Encodes an byte array to an base64 string.
	 * 
	 * @param data
	 *            The byte array to encode
	 * @return The base64 encoded string
	 */
	public static String encodeBase64(byte[] data) {
		return Base64.getEncoder().encodeToString(data);
	}

	/**
	 * Decodes an base64 encoded string to an byte array.
	 * 
	 * @param data
	 *            The base64 encoded string
	 * @return The byte array
	 */
	public static byte[] decodeBase64(String data) {
		return Base64.getDecoder().decode(data);
	}

	/**
	 * Generates a random string with the specified length.
	 * 
	 * @param length
	 *            The length of the random string
	 * @return The random string
	 */
	public static String generateRandomString(int length) {

		StringBuilder builder = new StringBuilder();
		for (int index = 0; index < length; index++) {
			builder.append(CHARS.charAt(secureRandom.nextInt(CHARS.length())));
		}

		return builder.toString();
	}

}