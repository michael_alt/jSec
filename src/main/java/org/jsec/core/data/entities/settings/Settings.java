package org.jsec.core.data.entities.settings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jsec.api.v1.config.security.ApiSecurityRole.AdministratorView;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * This class represents the application level settings.
 * 
 * @author alt
 *
 */
@Entity
@Table(name = "settings")
public class Settings {

	private int id;
	private String sessionSecret;
	private int sessionTimeout;
	private int passwordPolicyLevel;

	public Settings() {
		// The default constructor
	}

	@JsonView(AdministratorView.class)
	@Id
	@Column(name = "id", insertable = true, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "session_secret", insertable = true, updatable = true)
	public String getSessionSecret() {
		return sessionSecret;
	}

	public void setSessionSecret(String sessionSecret) {
		this.sessionSecret = sessionSecret;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "session_timeout", insertable = true, updatable = true)
	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "password_policy_level", insertable = true, updatable = true)
	public int getPasswordPolicyLevel() {
		return passwordPolicyLevel;
	}

	public void setPasswordPolicyLevel(int passwordPolicyLevel) {
		this.passwordPolicyLevel = passwordPolicyLevel;
	}

}