package org.jsec.core.data.entities.user;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.jsec.api.v1.config.security.ApiSecurityRole.AuthenticatedView;
import org.jsec.core.data.entities.role.Role;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * This class represents a single user.
 * 
 * @author alt
 *
 */
public class Account {

	private int id;
	private boolean active;
	private int type;
	private String username;
	private String displayName;
	private String mail;
	private boolean avatar;
	private String language;

	private Date createdAt;
	private Date updatedAt;

	private List<Role> roles;

	public Account() {
		this.roles = new LinkedList<>();
	}

	public Account(User user) {
		this.id = user.getId();
		this.active = user.isActive();
		this.type = user.getType();
		this.username = user.getUsername();
		this.displayName = user.getDisplayName();
		this.mail = user.getMail();
		this.avatar = user.isAvatar();
		this.language = user.getLanguage();

		this.createdAt = user.getCreatedAt();
		this.updatedAt = user.getUpdatedAt();

		this.roles = user.getRoles();
	}

	@JsonView(AuthenticatedView.class)
	public int getId() {
		return id;
	}

	@JsonView(AuthenticatedView.class)
	public boolean isActive() {
		return active;
	}

	@JsonView(AuthenticatedView.class)
	public int getType() {
		return type;
	}

	@JsonView(AuthenticatedView.class)
	public String getUsername() {
		return username;
	}

	@JsonView(AuthenticatedView.class)
	public String getDisplayName() {
		return displayName;
	}

	@JsonView(AuthenticatedView.class)
	public String getMail() {
		return mail;
	}

	@JsonView(AuthenticatedView.class)
	public boolean isAvatar() {
		return avatar;
	}

	@JsonView(AuthenticatedView.class)
	public String getLanguage() {
		return language;
	}

	@JsonView(AuthenticatedView.class)
	public Date getCreatedAt() {
		return createdAt;
	}

	@JsonView(AuthenticatedView.class)
	public Date getUpdatedAt() {
		return updatedAt;
	}

	@JsonView(AuthenticatedView.class)
	public List<Role> getRoles() {
		return roles;
	}

}