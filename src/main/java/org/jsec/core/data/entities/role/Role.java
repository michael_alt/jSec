package org.jsec.core.data.entities.role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jsec.api.v1.config.security.ApiSecurityRole.AuthenticatedView;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * This class represents a single role.
 * 
 * @author alt
 *
 */
@Entity
@Table(name = "roles")
public class Role {

	private int id;
	private String uniqueName;

	public Role() {
		// The default constructor
	}

	@JsonView(AuthenticatedView.class)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "unique_name", insertable = false, updatable = false)
	public String getUniqueName() {
		return uniqueName;
	}

	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

}