package org.jsec.core.data.entities.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents an user JSON Web Token.
 * 
 * @author alt
 *
 */
public class AuthToken {

	private String accessToken;
	private long expires;

	public AuthToken() {
		// Default constructor
	}

	@JsonProperty
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@JsonProperty
	public long getExpires() {
		return expires;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

}