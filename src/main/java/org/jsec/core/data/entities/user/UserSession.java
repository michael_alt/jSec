package org.jsec.core.data.entities.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jsec.api.v1.config.security.ApiSecurityRole.AuthenticatedView;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * This class represents a single user session.
 * 
 * @author alt
 *
 */
@Entity
@Table(name = "users_sessions")
public class UserSession {

	private int id;
	private int user;
	private String secret;
	private Date createdAt;
	private Date expiresAt;
	private String deviceIp;
	private String deviceType;
	private String deviceOs;
	private String deviceBrowser;

	public UserSession() {
		// The default constructor
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "user_id", insertable = true, updatable = false)
	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	@Column(name = "secret", insertable = true, updatable = false)
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "created_at", insertable = false, updatable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "expires_at", insertable = true, updatable = false)
	public Date getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "device_ip", insertable = true, updatable = false)
	public String getDeviceIp() {
		return deviceIp;
	}

	public void setDeviceIp(String deviceIp) {
		this.deviceIp = deviceIp;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "device_type", insertable = true, updatable = false)
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "device_os", insertable = true, updatable = false)
	public String getDeviceOs() {
		return deviceOs;
	}

	public void setDeviceOs(String deviceOs) {
		this.deviceOs = deviceOs;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "device_browser", insertable = true, updatable = false)
	public String getDeviceBrowser() {
		return deviceBrowser;
	}

	public void setDeviceBrowser(String deviceBrowser) {
		this.deviceBrowser = deviceBrowser;
	}

}