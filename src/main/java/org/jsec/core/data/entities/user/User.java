package org.jsec.core.data.entities.user;

import java.security.Principal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.jsec.api.v1.config.security.ApiSecurityRole.AdministratorView;
import org.jsec.api.v1.config.security.ApiSecurityRole.AuthenticatedView;
import org.jsec.core.data.entities.role.Role;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * This class represents a single user account.
 * 
 * @author alt
 *
 */
@Entity
@Table(name = "users")
public class User implements Principal {

	private int id;
	private boolean active;
	private int type;
	private String username;
	private String displayName;
	private String mail;
	private boolean avatar;
	private String language;

	private int lockoutAttempts;
	private Date lockoutTime;

	private Date createdAt;
	private Date updatedAt;

	private List<Role> roles;

	public User() {
		this.roles = new LinkedList<>();
	}

	@JsonView(AuthenticatedView.class)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "active", insertable = false, updatable = true)
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "type", insertable = true, updatable = false)
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "username", insertable = true, updatable = true)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "display_name", insertable = true, updatable = true)
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "mail", insertable = true, updatable = true)
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "avatar", insertable = true, updatable = true)
	public boolean isAvatar() {
		return avatar;
	}

	public void setAvatar(boolean avatar) {
		this.avatar = avatar;
	}

	@JsonView(AuthenticatedView.class)
	@Column(name = "language", insertable = true, updatable = true)
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "lockout_attempts", insertable = true, updatable = true)
	public int getLockoutAttempts() {
		return lockoutAttempts;
	}

	public void setLockoutAttempts(int lockoutAttempts) {
		this.lockoutAttempts = lockoutAttempts;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "lockout_time", insertable = true, updatable = true)
	public Date getLockoutTime() {
		return lockoutTime;
	}

	public void setLockoutTime(Date lockoutTime) {
		this.lockoutTime = lockoutTime;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "created_at", insertable = false, updatable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@JsonView(AdministratorView.class)
	@Column(name = "updated_at", insertable = false, updatable = false)
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@JsonView(AdministratorView.class)
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH,
			CascadeType.DETACH }, targetEntity = Role.class)
	@JoinTable(name = "users_roles", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id", unique = true) })
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@JsonIgnore
	@Transient
	@Override
	public String getName() {
		return null;
	}

}