package org.jsec.core.data.services.role;

import java.util.List;

import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.repositories.role.RoleRepository;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.util.error.ApplicationError;
import org.jsec.core.util.pagination.Pagination;

/**
 * This class is used to manage roles.
 * 
 * @author alt
 *
 */
public class RoleService {

	private RoleRepository repository;

	/**
	 * Default constructor of the class.
	 */
	public RoleService() {
		this.repository = new RoleRepository();
	}

	/**
	 * Returns a single role by it's unique id.
	 * 
	 * @param id
	 *            The unique id of the role
	 * @return The role
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Role getRole(int id) throws EntityNotFoundException, EntityPersistenceException {
		return this.repository.getById(id);
	}

	/**
	 * Returns a single role by it's unique name.
	 * 
	 * @param uniqueName
	 *            The unique name of the role
	 * @return The role
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Role getRole(String uniqueName) throws EntityNotFoundException, EntityPersistenceException {
		return this.repository.getByUniqueName(uniqueName);
	}

	/**
	 * Returns all roles.
	 * 
	 * @return The list of roles
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<Role> getAll() throws EntityPersistenceException {
		return this.repository.getAll();
	}

	/**
	 * Returns a limited list of roles that match the provided search criteria.
	 * 
	 * @param search
	 *            The search string
	 * @param page
	 *            The current page
	 * @param pageSize
	 *            The number of entries to return
	 * @return The list of roles
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public Pagination<Role> find(String search, int page, int pageSize) throws EntityPersistenceException {

		String localSearch = search;
		int localPage = page;
		int localPageSize = pageSize;

		if (localSearch == null) {
			localSearch = "";
		}

		if (localPage <= 0) {
			localPage = 0;
		} else {
			localPage = localPage - 1;
		}

		if (localPageSize <= 0) {
			localPageSize = 10;
		}

		return this.repository.find(localSearch, localPage, localPageSize);
	}

	/**
	 * Checks if the provided user is member of the role.
	 * 
	 * @param role
	 *            The role
	 * @param user
	 *            The user
	 * @return True if member, false if not
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public boolean isMemberOfRole(Role role, User user) throws EntityValidationException, EntityPersistenceException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("roleValidationEmptyUser", "Please provide a valid user entity."));
		}

		if (role == null || role.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("roleValidationEmptyRole", "Please provide a valid role entity."));
		}

		return this.repository.isUserMemberOfRole(role, user);
	}

}