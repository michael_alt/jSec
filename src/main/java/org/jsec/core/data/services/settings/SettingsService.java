package org.jsec.core.data.services.settings;

import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.data.entities.settings.Settings;
import org.jsec.core.data.repositories.settings.SettingsRepository;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.util.StringEncodingUtils;
import org.jsec.core.util.error.ApplicationError;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This class is used to manage application settings.
 * 
 * @author alt
 *
 */
public class SettingsService {

	private SettingsRepository repository;

	/**
	 * Default constructor of the class.
	 */
	public SettingsService() {
		this.repository = new SettingsRepository();
	}

	/**
	 * Returns the application settings.
	 * 
	 * @return The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Settings getSettings() throws EntityPersistenceException {
		return this.repository.get();
	}

	/**
	 * Create the application settings.
	 * 
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void create() throws EntityPersistenceException {

		Settings settings = new Settings();
		settings.setId(1);
		settings.setSessionSecret(StringEncodingUtils.generateRandomString(64));
		settings.setSessionTimeout(86400);
		settings.setPasswordPolicyLevel(1);

		this.repository.create(settings);
	}

	/**
	 * Updates the application settings.
	 * 
	 * @param settings
	 *            The application settings
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid parameter was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void update(Settings settings) throws EntityValidationException, EntityPersistenceException {

		if (settings == null || settings.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("settingValidationEmpty", "Please provide a valid settings entity."));
		}

		this.validate(settings);
		this.repository.update(settings);

		ConfigurationContext.getInstance().loadApplicationSettings();
	}

	/**
	 * Reloads the application settings.
	 * 
	 * @param settings
	 *            The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void reload(Settings settings) throws EntityPersistenceException {
		this.repository.reload(settings);
	}

	public void validate(Settings settings) throws EntityValidationException {

		if (settings.getSessionSecret() == null || settings.getSessionSecret().equals("")) {
			throw new EntityValidationException(new ApplicationError("invalidSessionSecretSetting",
					"Please provide a valid session secret, not 'null' or 'empty'."));
		}

		if (settings.getSessionTimeout() < 3600) {
			throw new EntityValidationException(new ApplicationError("invalidSessionTimeoutSetting",
					"Please provide a valid session timeout, larger than '3600' seconds."));
		}
	}

	public void updateEntity(Settings settings, JsonNode json) {

		if (json.has("sessionTimeout") && json.get("sessionTimeout").isInt()) {
			settings.setSessionTimeout(json.get("sessionTimeout").asInt());
		}

		if (json.has("passwordPolicyLevel") && json.get("passwordPolicyLevel").isInt()) {
			settings.setPasswordPolicyLevel(json.get("passwordPolicyLevel").asInt());
		}
	}

}