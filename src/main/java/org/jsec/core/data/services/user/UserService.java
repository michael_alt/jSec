package org.jsec.core.data.services.user;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.repositories.user.UserRepository;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.security.password.PasswordEncoder;
import org.jsec.core.storage.user.UserStorageProvider;
import org.jsec.core.util.error.ApplicationError;
import org.jsec.core.util.pagination.Pagination;
import org.jsec.core.util.validation.ValidationHelper;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This class is used to manage user accounts.
 * 
 * @author alt
 *
 */
public class UserService {

	private UserRepository repository;

	/**
	 * Default constructor of the class.
	 */
	public UserService() {
		this.repository = new UserRepository();
	}

	/**
	 * Returns a single user account by it's unique id.
	 * 
	 * @param id
	 *            The unique id of the user account
	 * @return The user account
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public User getUser(int id) throws EntityPersistenceException, EntityNotFoundException {
		return this.repository.getById(id);
	}

	/**
	 * Returns a single user account by it's unique username.
	 * 
	 * @param username
	 *            The unique username of the user
	 * @return The user
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public User getUser(String username) throws EntityPersistenceException {
		return this.repository.getByUsername(username);
	}

	/**
	 * Returns all user accounts.
	 * 
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<User> getAll() throws EntityPersistenceException {
		return this.repository.getAll();
	}

	/**
	 * Returns a list of user accounts that match the provided search criteria.
	 * 
	 * @param search
	 *            The search string
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public List<User> findUsers(String search) throws EntityPersistenceException {

		String localSearch = search;

		if (localSearch == null) {
			localSearch = "";
		}

		return this.repository.find(localSearch);
	}

	/**
	 * Returns a limited list of user accounts that match the provided search
	 * criteria.
	 * 
	 * @param search
	 *            The search string
	 * @param page
	 *            The current page
	 * @param pageSize
	 *            The number of entries to return
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public Pagination<User> findUsers(String search, int page, int pageSize) throws EntityPersistenceException {

		String localSearch = search;
		int localPage = page;
		int localPageSize = pageSize;

		if (localSearch == null) {
			localSearch = "";
		}

		if (localPage <= 0) {
			localPage = 0;
		} else {
			localPage = localPage - 1;
		}

		if (localPageSize <= 0) {
			localPageSize = 15;
		}

		return this.repository.find(localSearch, localPage, localPageSize);
	}

	/**
	 * Creates a new LDAP user account.
	 * 
	 * @param user
	 *            The new user account
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid parameter was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws EntityDuplicateException
	 *             Will be thrown if a user account with the same username or mail
	 *             already exists
	 */
	public void create(User user)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		if (user == null) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		user.setType(2);
		this.validate(user);
		this.repository.create(user);
	}

	/**
	 * Creates a new user account.
	 * 
	 * @param user
	 *            The new user account
	 * @param password
	 *            The users password
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid parameter was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws EntityDuplicateException
	 *             Will be thrown if a user account with the same username or mail
	 *             already exists
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the hashing algorithm is not available
	 * @throws InvalidKeySpecException
	 *             Will be thrown if an error during hashing occurred
	 */
	public void create(User user, String password) throws EntityValidationException, EntityPersistenceException,
			EntityDuplicateException, NoSuchAlgorithmException, InvalidKeySpecException {

		if (user == null) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		if (password == null || password.equals("")) {
			throw new EntityValidationException(new ApplicationError("userValidationEmptyPassword",
					"Please provide a valid password string, not null or empty."));
		}

		ValidationHelper.isValidPassword(password);

		PasswordEncoder encoder = new PasswordEncoder();
		String encodedPassword = encoder.encode(password);

		user.setType(1);
		this.validate(user);
		this.repository.create(user);
		this.repository.setPassword(user, encodedPassword);
	}

	/**
	 * Updates an existing user account.
	 * 
	 * @param user
	 *            The user account
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid parameter was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws EntityDublicateException
	 *             Will be thrown if a user account with the same username or mail
	 *             already exists
	 */
	public void update(User user)
			throws EntityValidationException, EntityPersistenceException, EntityDuplicateException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		this.validate(user);
		this.repository.update(user);
	}

	/**
	 * Sets a new password for the user account. The password must be encrypted.
	 * 
	 * @param user
	 *            The user account
	 * @param password
	 *            The new encrypted password string
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user account or password was
	 *             provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 */
	public void setPassword(User user, String password) throws EntityValidationException, EntityPersistenceException,
			NoSuchAlgorithmException, InvalidKeySpecException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		if (password == null || password.equals("")) {
			throw new EntityValidationException(new ApplicationError("userValidationEmptyPassword",
					"Please provide a valid password string, not null or empty."));
		}

		ValidationHelper.isValidPassword(password);

		PasswordEncoder encoder = new PasswordEncoder();
		String encodedPassword = encoder.encode(password);

		this.repository.setPassword(user, encodedPassword);
	}

	/**
	 * Deletes an existing user account.
	 * 
	 * @param user
	 *            The user account to delete
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void remove(User user) throws EntityValidationException, EntityPersistenceException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		this.repository.remove(user);
		new UserStorageProvider().removeUserAvatar(user);
	}

	/**
	 * Adds the user as member of the role.
	 * 
	 * @param user
	 *            The user
	 * @param role
	 *            The role
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void addRole(User user, Role role) throws EntityValidationException, EntityPersistenceException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		if (role == null || role.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyRole", "Please provide a valid role entity."));
		}

		this.repository.addRole(user, role);
	}

	/**
	 * Removes the user from the role.
	 * 
	 * @param user
	 *            The user
	 * @param role
	 *            The role
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void removeRole(User user, Role role) throws EntityValidationException, EntityPersistenceException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyUser", "Please provide a valid user entity."));
		}

		if (role == null || role.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("userValidationEmptyRole", "Please provide a valid role entity."));
		}

		this.repository.removeRole(user, role);
	}

	public void validate(User user) throws EntityValidationException {

		if (user.getUsername() == null || user.getUsername().equals("")) {
			throw new EntityValidationException(new ApplicationError("userValidationUsernameEmpty",
					"Please provide an valid 'username', not null or empty."));
		} else if (!ValidationHelper.isValidUsername(user.getUsername())) {
			throw new EntityValidationException(new ApplicationError("userValidationUsernamePattern",
					"Please provide an valid 'username', the username may only include numbers and chars."));
		}

		if (user.getDisplayName() == null || user.getDisplayName().equals("")) {
			throw new EntityValidationException(new ApplicationError("userValidationNameEmpty",
					"Please provide an valid 'name', not null or empty."));
		}

		if (user.getMail() == null || user.getMail().equals("")) {
			throw new EntityValidationException(new ApplicationError("userValidationMailEmpty",
					"Please provide an valid 'mail', not null or empty."));
		} else if (!ValidationHelper.isValidMailAddress(user.getMail())) {
			throw new EntityValidationException(new ApplicationError("userValidationMailPattern",
					"Please provide an valid 'mail', the mail does not match an mail pattern."));
		}
	}

	public User createEntity(JsonNode json) {

		User user = new User();

		if (json.has("type") && json.get("type").isInt()) {
			user.setType(json.get("type").asInt());
		}

		if (json.has("active") && json.get("active").isBoolean()) {
			user.setActive(json.get("active").asBoolean());
		}

		if (json.has("username")) {
			user.setUsername(ValidationHelper.getNullableString(json.get("username")));
		}

		if (json.has("displayName")) {
			user.setDisplayName(ValidationHelper.getNullableString(json.get("displayName")));
		}

		if (json.has("mail")) {
			user.setMail(ValidationHelper.getNullableString(json.get("mail")));
		}

		if (json.has("language")) {
			user.setLanguage(ValidationHelper.getNullableString(json.get("language")));
		}

		return user;
	}

	public void updateEntity(User user, JsonNode json) {

		if (json.has("active") && json.get("active").isBoolean()) {
			user.setActive(json.get("active").asBoolean());
		}

		if (json.has("displayName")) {
			user.setDisplayName(ValidationHelper.getNullableString(json.get("displayName")));
		}

		if (json.has("mail")) {
			user.setMail(ValidationHelper.getNullableString(json.get("mail")));
		}

		if (json.has("language")) {
			user.setLanguage(ValidationHelper.getNullableString(json.get("language")));
		}
	}

}