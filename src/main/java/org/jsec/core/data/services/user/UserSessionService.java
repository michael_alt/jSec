package org.jsec.core.data.services.user;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.data.entities.settings.Settings;
import org.jsec.core.data.entities.user.AuthToken;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.entities.user.UserSession;
import org.jsec.core.data.repositories.user.UserSessionRepository;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.exceptions.EntityValidationException;
import org.jsec.core.util.error.ApplicationError;

import eu.bitwalker.useragentutils.UserAgent;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * This class is used to manage user sessions.
 * 
 * @author alt
 *
 */
public class UserSessionService {

	private static final Logger log = LogManager.getLogger(UserSessionService.class);
	private static final String ENCRYPTION_ALGORITHM = "HS256";

	private Settings settings;
	private UserService userService;
	private UserSessionRepository repository;

	/**
	 * Default constructor of the class.
	 */
	public UserSessionService() {
		this.settings = ConfigurationContext.getInstance().getApplicationSettings();
		this.userService = new UserService();
		this.repository = new UserSessionRepository();
	}

	/**
	 * Verifies that the specified JWT token is valid.
	 * 
	 * @param token
	 *            The JWT token
	 * @return True or False
	 */
	public boolean checkToken(String token) {

		try {

			byte[] decodedKey = this.settings.getSessionSecret().getBytes();
			SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length,
					UserSessionService.ENCRYPTION_ALGORITHM);

			JwtParser parser = Jwts.parser();
			parser.setSigningKey(originalKey);

			Claims claims = parser.parseClaimsJws(token).getBody();
			if ((claims.getSubject() != null) && (claims.getExpiration().getTime() > new Date().getTime())) {
				return true;
			}
		} catch (Exception e) {
			log.error("UserSessionService.checkToken()", e);
		}

		return false;
	}

	/**
	 * Issues a new JWT token for the specified user account.
	 * 
	 * @param user
	 *            The user account
	 * @param remoteAddress
	 *            The address the request was send from
	 * @param userAgentString
	 *            The user agent string of the request
	 * @return The JWT token
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public AuthToken issueToken(User user, String remoteAddress, String userAgentString)
			throws EntityValidationException, EntityPersistenceException {

		byte[] decodedKey = this.settings.getSessionSecret().getBytes();
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length,
				UserSessionService.ENCRYPTION_ALGORITHM);

		Date expires = new Date();
		long ttl = this.settings.getSessionTimeout();
		long newTime = new Date().getTime() + (ttl * 1000);
		expires.setTime(newTime);

		UserSession userSession = new UserSession();
		userSession.setSecret(UUID.randomUUID().toString());
		userSession.setUser(user.getId());
		userSession.setExpiresAt(expires);

		if (userAgentString != null) {
			UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
			userSession.setDeviceIp(remoteAddress);
			userSession.setDeviceType(userAgent.getOperatingSystem().getDeviceType().getName());
			userSession.setDeviceBrowser(
					userAgent.getBrowser().getName() + " " + userAgent.getBrowserVersion().getVersion());
			userSession.setDeviceOs(userAgent.getOperatingSystem().getName());
		}

		JwtBuilder builder = Jwts.builder();
		builder.setIssuedAt(new Date());
		builder.setExpiration(expires);
		builder.setSubject(userSession.getSecret());

		AuthToken authToken = new AuthToken();
		authToken.setAccessToken(builder.signWith(SignatureAlgorithm.HS256, originalKey).compact());
		authToken.setExpires(expires.getTime());

		this.repository.create(userSession);
		return authToken;
	}

	/**
	 * Returns the user account for the specified JWT token.
	 * 
	 * @param token
	 *            The JWT token
	 * @return The user account or null
	 */
	public User getUserByToken(String token) {

		try {

			byte[] decodedKey = this.settings.getSessionSecret().getBytes();
			SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length,
					UserSessionService.ENCRYPTION_ALGORITHM);

			JwtParser parser = Jwts.parser();
			parser.setSigningKey(originalKey);

			Claims claims = parser.parseClaimsJws(token).getBody();

			UserSession userToken = getSession(claims.getSubject());
			if (userToken != null) {
				return this.userService.getUser(userToken.getUser());
			}
		} catch (Exception e) {
			log.error("UserSessionService.getUserByToken()", e);
		}

		return null;
	}

	/**
	 * Returns a single user sessions by it's unique id.
	 * 
	 * @param user
	 *            The user of the session
	 * @param id
	 *            The unique id of the session
	 * @return The user session
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public UserSession getSession(User user, int id) throws EntityNotFoundException, EntityPersistenceException {
		return this.repository.getById(user, id);
	}

	/**
	 * Returns a single user session by it's unique id.
	 * 
	 * @param secret
	 *            The unique secret of the user session
	 * @return The user session
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public UserSession getSession(String secret) throws EntityPersistenceException {
		return this.repository.getBySecret(secret);
	}

	/**
	 * Gets all sessions of the specified user.
	 * 
	 * @param user
	 *            The user
	 * @return The list of sessions
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<UserSession> getSessions(User user) throws EntityPersistenceException {
		return this.repository.getAll(user);
	}

	/**
	 * Removes the specified user session.
	 * 
	 * @param userSession
	 *            The user session to remove
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid session object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void remove(UserSession userSession) throws EntityValidationException, EntityPersistenceException {

		if (userSession == null) {
			throw new EntityValidationException(
					new ApplicationError("sessionValidationEmptySession", "Please provide a valid session entity."));
		}

		this.repository.remove(userSession);
	}

	/**
	 * Removes all sessions of the specified user.
	 * 
	 * @param user
	 *            The user
	 * @throws EntityValidationException
	 *             Will be thrown if an invalid user object was provided
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void removeAll(User user) throws EntityValidationException, EntityPersistenceException {

		if (user == null || user.getId() <= 0) {
			throw new EntityValidationException(
					new ApplicationError("sessionValidationEmptyUser", "Please provide a valid user entity."));
		}

		this.repository.removeAll(user);
	}

	/**
	 * Deletes all user session that have expired before the provided date.
	 * 
	 * @param date
	 *            The date
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void cleanExpiredSessions(Date date) throws EntityPersistenceException {
		this.repository.cleanExpiredSessions(date);
	}

}