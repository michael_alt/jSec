package org.jsec.core.data.repositories.settings;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.data.entities.settings.Settings;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.util.error.ApplicationError;

/**
 * This class is used to manage the application settings in the database.
 * 
 * @author alt
 *
 */
public class SettingsRepository {

	private static final Logger log = LogManager.getLogger(SettingsRepository.class);

	/**
	 * Returns the application settings.
	 * 
	 * @return The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Settings get() throws EntityPersistenceException {

		EntityManager entityManager = null;
		Settings settings = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			settings = entityManager.find(Settings.class, 1);

			if (settings == null) {
				throw new NoResultException();
			}
		} catch (NoResultException e) {
			log.error("SettingsRepository.getSettings()", e);
			throw new EntityPersistenceException(
					new ApplicationError("settingsNotFound", "The settings could not be found."));
		} catch (Exception e) {
			log.error("SettingsRepository.getSettings()", e);
			throw new EntityPersistenceException(
					new ApplicationError("settingsLoading", "The settings could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
		return settings;
	}

	/**
	 * Installs the default application settings.
	 * 
	 * @param settings
	 *            The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void create(Settings settings) throws EntityPersistenceException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.persist(settings);

			entityManager.getTransaction().commit();
			entityManager.refresh(settings);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("SettingsRepository.create()", e);
			throw new EntityPersistenceException(
					new ApplicationError("settingsCreate", "The settings could not be created in the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Updates the application settings.
	 * 
	 * @param settings
	 *            The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void update(Settings settings) throws EntityPersistenceException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.merge(settings);

			entityManager.getTransaction().commit();
			entityManager.refresh(settings);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("SettingsRepository.update()", e);
			throw new EntityPersistenceException(
					new ApplicationError("settingsUpdate", "The settings could not be updated in the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Refreshes the application settings.
	 * 
	 * @param settings
	 *            The application settings
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void reload(Settings settings) throws EntityPersistenceException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.refresh(settings);

		} catch (Exception e) {
			log.error("SettingsRepository.reload()", e);
			throw new EntityPersistenceException(
					new ApplicationError("settingsReload", "The settings could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

}