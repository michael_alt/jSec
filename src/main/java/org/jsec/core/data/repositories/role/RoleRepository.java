package org.jsec.core.data.repositories.role;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.util.error.ApplicationError;
import org.jsec.core.util.pagination.Pagination;

/**
 * This class is used to manage roles in the database.
 * 
 * @author alt
 *
 */
public class RoleRepository {

	private static final Logger log = LogManager.getLogger(RoleRepository.class);

	/**
	 * Returns a single role by it's unique id.
	 * 
	 * @param id
	 *            The unique id of the role
	 * @return The role
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Role getById(int id) throws EntityNotFoundException, EntityPersistenceException {

		EntityManager entityManager = null;
		Role role = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			role = entityManager.find(Role.class, id);

			if (role == null) {
				throw new NoResultException();
			}
		} catch (NoResultException e) {
			log.error("RoleRepository.getById()", e);
			throw new EntityNotFoundException(
					new ApplicationError("roleNotFound", "The role with the specified id could not be found."));
		} catch (Exception e) {
			log.error("RoleRepository.getById()", e);
			throw new EntityPersistenceException(
					new ApplicationError("roleLoading", "The role could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return role;
	}

	/**
	 * Returns a single role by it's unique name.
	 * 
	 * @param uniqueName
	 *            The unique name of the role
	 * @return The role
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public Role getByUniqueName(String uniqueName) throws EntityNotFoundException, EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<Role> query = null;

		Role role = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery("FROM Role r WHERE r.uniqueName=:uniqueName", Role.class);
			query.setParameter("uniqueName", uniqueName);

			role = query.getSingleResult();
			if (role == null) {
				throw new NoResultException();
			}
		} catch (NoResultException e) {
			log.error("RoleRepository.getByUniqueName()", e);
			throw new EntityNotFoundException(new ApplicationError("roleNotFound",
					"The role with the specified unique name could not be found."));
		} catch (Exception e) {
			log.error("RoleRepository.getByUniqueName()", e);
			throw new EntityPersistenceException(
					new ApplicationError("roleLoading", "The role could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return role;
	}

	/**
	 * Returns all roles.
	 * 
	 * @return The list of roles
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<Role> getAll() throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<Role> query = null;

		LinkedList<Role> roles = new LinkedList<>();

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery("FROM Role r ORDER BY r.uniqueName", Role.class);
			roles.addAll(query.getResultList());

		} catch (Exception e) {
			log.error("RoleRepository.getAll()", e);
			throw new EntityPersistenceException(
					new ApplicationError("roleLoading", "The roles could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return roles;
	}

	/**
	 * Returns a limited list of roles that match the provided search criteria.
	 * 
	 * @param search
	 *            The search string
	 * @param page
	 *            The current page
	 * @param pageSize
	 *            The number of entries to return
	 * @return The list of roles
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	@SuppressWarnings("unchecked")
	public Pagination<Role> find(String search, int page, int pageSize) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		Pagination<Role> container = new Pagination<>();
		LinkedList<Role> objects = new LinkedList<>();

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery("SELECT COUNT(r.id) FROM Role r WHERE r.uniqueName LIKE :search");
			query.setParameter("search", "%" + search + "%");

			container.setPage((long) page + 1);
			container.setPageSize(pageSize);
			container.setEntriesTotal((long) query.getSingleResult());

			if (container.getEntriesTotal() > 0) {
				long pages = Math.round(Math.ceil(((double) container.getEntriesTotal() / (double) pageSize)));
				container.setPages(pages);
			} else {
				container.setPages(1);
			}

			query = entityManager.createQuery("FROM Role r WHERE r.uniqueName LIKE :search ORDER BY r.uniqueName",
					Role.class);
			query.setParameter("search", "%" + search + "%");
			query.setFirstResult(page * pageSize);
			query.setMaxResults(pageSize);

			objects.addAll(query.getResultList());
			container.setContent(objects);

		} catch (Exception e) {
			log.error("RoleRepository.find()", e);
			throw new EntityPersistenceException(
					new ApplicationError("roleLoading", "The roles could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return container;
	}

	/**
	 * Checks if the provided user is member of the role.
	 * 
	 * @param role
	 *            The role
	 * @param user
	 *            The user
	 * @return True if member, false if not
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public boolean isUserMemberOfRole(Role role, User user) throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<Long> query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery(
					"SELECT COUNT(u.id) FROM User u JOIN u.roles r WHERE u.id=:user AND r.id=:role", Long.class);
			query.setParameter("role", role.getId());
			query.setParameter("user", user.getId());

			return ((long) query.getSingleResult() > 0);
		} catch (Exception e) {
			log.error("RoleRepository.isUserMemberOfRole()", e);
			throw new EntityPersistenceException(
					new ApplicationError("roleLoading", "The role could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

}