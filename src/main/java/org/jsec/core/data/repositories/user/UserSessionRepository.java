package org.jsec.core.data.repositories.user;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.data.entities.user.UserSession;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.util.error.ApplicationError;

/**
 * This class is used to manage user sessions in the database.
 * 
 * @author alt
 *
 */
public class UserSessionRepository {

	private static final Logger log = LogManager.getLogger(UserSessionRepository.class);

	/**
	 * Returns a single user session by it's unique id.
	 * 
	 * @param user
	 *            The user of the session
	 * @param id
	 *            The unique id of the session
	 * @return The user session
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public UserSession getById(User user, int id) throws EntityNotFoundException, EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<UserSession> query = null;

		UserSession userSession = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery("FROM UserSession s WHERE s.user=:user AND s.id=:id", UserSession.class);
			query.setParameter("user", user.getId());
			query.setParameter("id", id);

			userSession = (UserSession) query.getSingleResult();
			if (userSession == null) {
				throw new NoResultException();
			}
		} catch (NoResultException e) {
			log.error("UserSessionRepository.getById()", e);
			throw new EntityNotFoundException(
					new ApplicationError("sessionsNotFound", "The session with the specified id could not be found."));
		} catch (Exception e) {
			log.error("UserSessionRepository.getById()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionLoading", "The session could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return userSession;
	}

	/**
	 * Returns a single user session by it's unique secret.
	 * 
	 * @param secret
	 *            The unique secret of the user session
	 * @return The user session
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public UserSession getBySecret(String secret) throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<UserSession> query = null;

		UserSession userSession = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery("FROM UserSession s WHERE s.secret=:secret", UserSession.class);
			query.setParameter("secret", secret);

			userSession = (UserSession) query.getSingleResult();
		} catch (NoResultException e) {
			// Prevent no result exception from being thrown
		} catch (Exception e) {
			log.error("UserSessionRepository.getBySecret()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionLoading", "The session could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return userSession;
	}

	/**
	 * Gets all sessions of the specified user.
	 * 
	 * @param user
	 *            The user session
	 * @return The list of sessions
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<UserSession> getAll(User user) throws EntityPersistenceException {

		EntityManager entityManagerm = null;
		TypedQuery<UserSession> query = null;

		List<UserSession> sessions = new LinkedList<>();

		try {

			entityManagerm = PersistenceProvider.getInstance().createEntityManager();

			query = entityManagerm.createQuery("FROM UserSession s WHERE s.user=:user", UserSession.class);
			query.setParameter("user", user.getId());
			sessions.addAll(query.getResultList());

		} catch (Exception e) {
			log.error("UserSessionRepository.getById()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionLoading", "The sessions could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManagerm);
		}

		return sessions;
	}

	/**
	 * Creates a new user session.
	 * 
	 * @param userSession
	 *            The new user session
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void create(UserSession userSession) throws EntityPersistenceException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.persist(userSession);

			entityManager.getTransaction().commit();
			entityManager.refresh(userSession);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserSessionRepository.create()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionCreate", "The session could not be created."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Removes the specified user session.
	 * 
	 * @param userSession
	 *            The user session to remove
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void remove(UserSession userSession) throws EntityPersistenceException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.remove(userSession);

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserSessionRepository.remove()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionRemove", "The session could not be deleted."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Removes all sessions of the specified user.
	 * 
	 * @param user
	 *            The user
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void removeAll(User user) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			query = entityManager.createQuery("DELETE FROM UserSession s WHERE s.user=:user");
			query.setParameter("user", user.getId());
			query.executeUpdate();

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserSessionRepository.removeAll()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionRemove", "The sessions could not be deleted."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Deletes all user session that have expired before the provided date.
	 * 
	 * @param date
	 *            The date
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void cleanExpiredSessions(Date date) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			query = entityManager.createQuery("DELETE FROM UserSession s WHERE s.expiresAt <= :date");
			query.setParameter("date", date);
			query.executeUpdate();

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserSessionRepository.removeExpired()", e);
			throw new EntityPersistenceException(
					new ApplicationError("sessionRemove", "The sessions could not be deleted."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

}