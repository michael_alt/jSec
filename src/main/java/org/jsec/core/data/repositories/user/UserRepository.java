package org.jsec.core.data.repositories.user;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.jsec.core.data.entities.role.Role;
import org.jsec.core.data.entities.user.User;
import org.jsec.core.exceptions.EntityDuplicateException;
import org.jsec.core.exceptions.EntityNotFoundException;
import org.jsec.core.exceptions.EntityPersistenceException;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.util.error.ApplicationError;
import org.jsec.core.util.pagination.Pagination;

/**
 * This class is used to manage user accounts in the database.
 * 
 * @author alt
 *
 */
public class UserRepository {

	private static final Logger log = LogManager.getLogger(UserRepository.class);

	/**
	 * Returns a single user account by it's unique id.
	 * 
	 * @param id
	 *            The unique id of the user account
	 * @return The user account
	 * @throws EntityNotFoundException
	 *             Will be thrown if no entity was found
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public User getById(int id) throws EntityNotFoundException, EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<User> query = null;

		User user = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			query = entityManager.createQuery("FROM User u WHERE u.id=:id", User.class);
			query.setParameter("id", id);

			user = query.getSingleResult();
			if (user == null) {
				throw new NoResultException();
			}
		} catch (NoResultException e) {
			log.error("UserRepository.getById()", e);
			throw new EntityNotFoundException(
					new ApplicationError("userNotFound", "The user with the specified id could not be found."));
		} catch (Exception e) {
			log.error("UserRepository.getById()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userLoading", "The user could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return user;
	}

	/**
	 * Returns a single user account by it's unique username.
	 * 
	 * @param username
	 *            The unique username of the user
	 * @return The user
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public User getByUsername(String username) throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<User> query = null;

		User user = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			query = entityManager.createQuery("FROM User u WHERE u.username=:username", User.class);
			query.setParameter("username", username);

			user = query.getSingleResult();
		} catch (NoResultException e) {
			// Prevent no result exception from being thrown
		} catch (Exception e) {
			log.error("UserRepository.getByUsername()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userLoading", "The user could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return user;
	}

	/**
	 * Returns all user accounts.
	 * 
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public List<User> getAll() throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<User> query = null;

		LinkedList<User> users = new LinkedList<>();

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			query = entityManager.createQuery("SELECT u FROM User u ORDER BY u.displayName", User.class);

			users.addAll(query.getResultList());
		} catch (Exception e) {
			log.error("UserRepository.getAll()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userLoading", "The users could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return users;
	}

	/**
	 * Returns a list of user accounts that match the provided search criteria.
	 * 
	 * @param search
	 *            The search string
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	public List<User> find(String search) throws EntityPersistenceException {

		EntityManager entityManager = null;
		TypedQuery<User> query = null;

		LinkedList<User> users = new LinkedList<>();

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			query = entityManager.createQuery(
					"SELECT u FROM User u WHERE u.username LIKE :search OR u.mail LIKE :search OR u.displayName LIKE :search ORDER BY u.displayName",
					User.class);
			query.setParameter("search", "%" + search + "%");

			users.addAll(query.getResultList());
		} catch (Exception e) {
			log.error("UserRepository.find()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userLoading", "The users could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return users;
	}

	/**
	 * Returns a limited list of user accounts that match the provided search
	 * criteria.
	 * 
	 * @param search
	 *            The search string
	 * @param page
	 *            The current page
	 * @param pageSize
	 *            The number of entries to return
	 * @return The list of user accounts
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error occurs
	 */
	@SuppressWarnings("unchecked")
	public Pagination<User> find(String search, int page, int pageSize) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		Pagination<User> container = new Pagination<>();
		LinkedList<User> objects = new LinkedList<>();

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();

			query = entityManager.createQuery(
					"SELECT COUNT(u.id) FROM User u WHERE u.username LIKE :search OR u.mail LIKE :search OR u.displayName LIKE :search");
			query.setParameter("search", "%" + search + "%");

			container.setPage((long) page + 1);
			container.setPageSize(pageSize);
			container.setEntriesTotal((long) query.getSingleResult());

			if (container.getEntriesTotal() > 0) {
				long pages = Math.round(Math.ceil(((double) container.getEntriesTotal() / (double) pageSize)));
				container.setPages(pages);
			} else {
				container.setPages(1);
			}

			query = entityManager.createQuery(
					"SELECT u FROM User u WHERE u.username LIKE :search OR u.mail LIKE :search OR u.displayName LIKE :search ORDER BY u.displayName",
					User.class);
			query.setParameter("search", "%" + search + "%");
			query.setFirstResult(page * pageSize);
			query.setMaxResults(pageSize);

			objects.addAll(query.getResultList());
			container.setContent(objects);

		} catch (Exception e) {
			log.error("UserRepository.find()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userLoading", "The users could not be loaded from the database."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return container;
	}

	/**
	 * Creates a new user account.
	 * 
	 * @param user
	 *            The new user account
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws EntityDuplicateException
	 *             Will be thrown if a user account with the same username already
	 *             exists
	 */
	public void create(User user) throws EntityPersistenceException, EntityDuplicateException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.persist(user);

			entityManager.getTransaction().commit();
			entityManager.refresh(user);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.create()", e);
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new EntityDuplicateException(new ApplicationError("userCreateDuplicate",
						"The user could not be created because a user with the same username or mail already exists."));
			} else {
				throw new EntityPersistenceException(
						new ApplicationError("userCreate", "The user could not be created."));
			}
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Updates an existing user account.
	 * 
	 * @param user
	 *            The user account
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 * @throws EntityDuplicateException
	 *             Will be thrown if a user account with the same username already
	 *             exists
	 */
	public void update(User user) throws EntityPersistenceException, EntityDuplicateException {

		EntityManager entityManager = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			entityManager.merge(user);

			entityManager.getTransaction().commit();
			entityManager.refresh(user);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.update()", e);
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new EntityDuplicateException(new ApplicationError("userUpdateDuplicate",
						"The user could not be updated because a user with the same username or mail already exists."));
			} else {
				throw new EntityPersistenceException(
						new ApplicationError("userUpdate", "The user could not be update."));
			}
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Sets a new password for the user account. The password must be encrypted.
	 * 
	 * @param user
	 *            The user account
	 * @param password
	 *            The new encrypted password string
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void setPassword(User user, String password) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			query = entityManager.createNativeQuery("UPDATE users SET encoded_password=:password WHERE id=:id");
			query.setParameter("password", password);
			query.setParameter("id", user.getId());
			query.executeUpdate();

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.setPassword()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userPassword", "The password of the user could not be updated."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Deletes an existing user account.
	 * 
	 * @param user
	 *            The user account to delete
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void remove(User user) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			// Delete user sessions
			query = entityManager.createNativeQuery("DELETE FROM users_sessions WHERE user_id=:id");
			query.setParameter("id", user.getId());
			query.executeUpdate();

			// Delete user sessions
			query = entityManager.createNativeQuery("DELETE FROM users_roles WHERE user_id=:id");
			query.setParameter("id", user.getId());
			query.executeUpdate();

			// Delete user groups
			query = entityManager.createNativeQuery("DELETE FROM groups_members WHERE user_id=:id");
			query.setParameter("id", user.getId());
			query.executeUpdate();

			// Delete user
			entityManager.remove(user);

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.remove()", e);
			throw new EntityPersistenceException(new ApplicationError("userRemove", "The user could not be deleted."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Adds the user as member of the role.
	 * 
	 * @param user
	 *            The user
	 * @param role
	 *            The role
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void addRole(User user, Role role) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			query = entityManager.createNativeQuery("INSERT INTO users_roles(user_id,role_id) VALUES(:user,:role)");
			query.setParameter("user", user.getId());
			query.setParameter("role", role.getId());
			query.executeUpdate();

			entityManager.getTransaction().commit();
			entityManager.refresh(user);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.addRole()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userAddRole", "The role could not be added to the user."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

	/**
	 * Removes the user from the role.
	 * 
	 * @param user
	 *            The user
	 * @param role
	 *            The role
	 * @throws EntityPersistenceException
	 *             Will be thrown if an error during the database operation occurs
	 */
	public void removeRole(User user, Role role) throws EntityPersistenceException {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			entityManager.getTransaction().begin();

			query = entityManager.createNativeQuery("DELETE FROM users_roles WHERE user_id=:user AND role_id=:role");
			query.setParameter("user", user.getId());
			query.setParameter("role", role.getId());
			query.executeUpdate();

			entityManager.getTransaction().commit();
			entityManager.refresh(user);

		} catch (Exception e) {

			if (entityManager != null && entityManager.getTransaction() != null
					&& entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

			log.error("UserRepository.removeRole()", e);
			throw new EntityPersistenceException(
					new ApplicationError("userRemoveRole", "The role could not be removed from the user."));
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}
	}

}