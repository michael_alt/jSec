package org.jsec.core.security.auth.database;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsec.core.persistence.PersistenceProvider;
import org.jsec.core.security.auth.AuthProvider;
import org.jsec.core.security.password.PasswordEncoder;

/**
 * This class is a specific implementation of the Authentication connector
 * interface. It authenticates users against the application database.
 * 
 * @author alt
 *
 */
public class DatabaseAuthProvider implements AuthProvider {

	private static final Logger log = LogManager.getLogger(DatabaseAuthProvider.class);

	private PasswordEncoder passwordEncoder;

	/**
	 * The default constructor of the class.
	 */
	public DatabaseAuthProvider() {
		this.passwordEncoder = new PasswordEncoder();
	}

	@Override
	public boolean authenticate(String username, String password) {

		EntityManager entityManager = null;
		Query query = null;

		try {

			entityManager = PersistenceProvider.getInstance().createEntityManager();
			query = entityManager.createNativeQuery(
					"SELECT encoded_password FROM users WHERE active=1 AND type=1 AND username=:username");
			query.setParameter("username", username);

			String encodedPassword = (String) query.getSingleResult();
			if (encodedPassword != null && this.passwordEncoder.verify(password, encodedPassword)) {
				return true;
			}
		} catch (NoResultException e) {
			// Catch so it wont be logged
		} catch (Exception e) {
			log.error("AuthProviderDatabase.authenticate()", e);
		} finally {
			PersistenceProvider.getInstance().close(entityManager);
		}

		return false;
	}

}