package org.jsec.core.security.auth;

/**
 * Specification of the authentication connector interface for database users.
 * 
 * @author alt
 *
 */
public interface AuthProvider {

	/**
	 * Authenticates the user and provides a valid AuthToken if the authentication
	 * was successful.
	 * 
	 * @param properties
	 *            The properties required for the specific authentication method
	 * @return The valid AuthToken or null
	 */
	public boolean authenticate(String username, String password);

}