package org.jsec.core.security.cert;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.encoders.Hex;
import org.jsec.core.configuration.ConfigurationContext;
import org.jsec.core.configuration.server.ServerConfiguration;

/**
 * This class is used to manage the trust and keystore of the application.
 * 
 * @author alt
 *
 */
public class CertStoreUtil {

	private static final Logger log = LogManager.getLogger(CertStoreUtil.class);

	private ServerConfiguration configuration;

	/**
	 * The default constructor of the class.
	 */
	public CertStoreUtil() {
		this.configuration = ConfigurationContext.getInstance().getServerConfiguration();
	}

	/**
	 * Constructor that manually loads the server configuration.
	 * 
	 * @param configuration
	 *            The configuration
	 */
	public CertStoreUtil(ServerConfiguration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Loads the specified keystore file from the file system.
	 * 
	 * @param file
	 *            The keystore file to load
	 * @param password
	 *            The password of the keystore file
	 * @return The loaded keystore file
	 */
	private KeyStore loadKeyStore(File file, String password) {

		KeyStore keyStore = null;
		FileInputStream inputStream = null;

		try {

			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			if (file.exists()) {
				inputStream = new FileInputStream(file);
				keyStore.load(inputStream, password.toCharArray());
			} else {
				keyStore.load(null, password.toCharArray());
			}
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			log.error("CertStoreUtil.loadKeyStore()", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("CertStoreUtil.loadKeyStore()", e);
				}
			}
		}

		return keyStore;
	}

	/**
	 * Writes the keystore file to the specified file.
	 * 
	 * @param keyStore
	 *            The keystore to write
	 * @param file
	 *            The path of the file
	 * @param password
	 *            The password of the keystore
	 * @return True if successful, false if not
	 */
	private boolean storeKeyStore(KeyStore keyStore, File file, String password) {

		FileOutputStream outputStream = null;

		try {

			outputStream = new FileOutputStream(file);
			keyStore.store(outputStream, password.toCharArray());

			return true;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			log.error("CertStoreUtil.storeKeyStore()", e);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					log.error("CertStoreUtil.storeKeyStore()", e);
				}
			}
		}

		return false;
	}

	/**
	 * Returns the TrustManager of the default SSL Context and initializes it with
	 * the specified truststore.
	 * 
	 * @param trustStore
	 *            The truststore to load or null
	 * @return The default trust manager
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the provided algorithm was not found
	 * @throws KeyStoreException
	 *             Will be thrown if an invalid truststore was provided
	 */
	private X509TrustManager getDefaultTrustManager(KeyStore trustStore)
			throws NoSuchAlgorithmException, KeyStoreException {

		TrustManagerFactory tmFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmFactory.init((KeyStore) trustStore);

		for (TrustManager tm : tmFactory.getTrustManagers()) {
			if (tm instanceof X509TrustManager) {
				return (X509TrustManager) tm;
			}
		}

		return null;
	}

	/**
	 * Generates a new RSA private public key pair.
	 * 
	 * @param keyLength
	 *            The length of the private key
	 * @return The key pair
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the RSA algorithm could not be found on the
	 *             system
	 */
	private KeyPair generateKeyPair(int keyLength) throws NoSuchAlgorithmException {

		if (keyLength < 4096) {
			keyLength = 4096;
		}

		KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
		keyGenerator.initialize(4096, SecureRandom.getInstanceStrong());

		return keyGenerator.generateKeyPair();
	}

	/**
	 * Creates a new SSL certificate from the private and public key pair.
	 * 
	 * @param keyPair
	 *            The public and private key pair
	 * @return The certificate
	 * @throws OperatorCreationException
	 *             Will be thrown if the certificate could not be signed with the
	 *             private key
	 * @throws CertificateException
	 *             Will be thrown if the certificate is not valid
	 * @throws IOException
	 *             Will be thrown if the certificate could not be created
	 */
	private X509Certificate createCertificate(KeyPair keyPair)
			throws OperatorCreationException, CertificateException, IOException {

		X500Name issuer = createIssuer();
		BigInteger serialNumber = generateSerialNumber();
		Date validFrom = generateValidFrom();
		Date validTo = generateValidTo();
		SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded());

		X509v3CertificateBuilder certificateBuilder = new X509v3CertificateBuilder(issuer, serialNumber, validFrom,
				validTo, issuer, publicKeyInfo);

		KeyUsage keyUsage = new KeyUsage(KeyUsage.keyEncipherment | KeyUsage.digitalSignature);
		ExtendedKeyUsage extKeyUsage = new ExtendedKeyUsage(KeyPurposeId.id_kp_serverAuth);

		certificateBuilder.addExtension(Extension.keyUsage, true, keyUsage);
		certificateBuilder.addExtension(Extension.extendedKeyUsage, false, extKeyUsage);
		certificateBuilder.addExtension(Extension.subjectAlternativeName, false, getSubjectAlternativeNames());
		certificateBuilder.addExtension(Extension.basicConstraints, false, new BasicConstraints(false));

		JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA512withRSA");
		ContentSigner signer = builder.build(keyPair.getPrivate());
		byte[] certificateData = certificateBuilder.build(signer).getEncoded();

		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		return (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(certificateData));
	}

	/**
	 * Creates a new subject name for the certificate.
	 * 
	 * @return The subject name
	 */
	private X500Name createIssuer() {

		InetAddress address = null;

		try {

			address = InetAddress.getLocalHost();
			return new X500Name("CN=" + address.getCanonicalHostName());
		} catch (UnknownHostException e) {
			return new X500Name("CN=ConnectedCookingServer");
		}
	}

	/**
	 * Creates the subject alternative names of the certificate.
	 * 
	 * @return The list of subject alternative names
	 */
	private DERSequence getSubjectAlternativeNames() {

		List<ASN1Encodable> subjectAlternativeNames = new ArrayList<>();

		InetAddress address = null;

		try {

			address = InetAddress.getLocalHost();
			subjectAlternativeNames.add(new GeneralName(GeneralName.iPAddress, address.getHostAddress()));
			subjectAlternativeNames.add(new GeneralName(GeneralName.dNSName, address.getHostName()));
			subjectAlternativeNames.add(new GeneralName(GeneralName.dNSName, address.getCanonicalHostName()));
		} catch (UnknownHostException e) {
			log.error("CertStoreUtil.getSubjectAlternativeNames()", e);
		}

		return new DERSequence(subjectAlternativeNames.toArray(new ASN1Encodable[subjectAlternativeNames.size()]));
	}

	/**
	 * Generates a new random serial number for the certificate.
	 * 
	 * @return The random serial number
	 */
	private BigInteger generateSerialNumber() {
		return BigInteger.probablePrime(120, new Random());
	}

	/**
	 * Generates a new new valid from date. Starting with the current date.
	 * 
	 * @return The valid from date
	 */
	private Date generateValidFrom() {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR, 0);

		return calendar.getTime();
	}

	/**
	 * Generates a new valid to date. Starting with the current date.
	 * 
	 * @return The valid to date
	 */
	private Date generateValidTo() {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR, 0);
		calendar.add(Calendar.DAY_OF_YEAR, 90);

		return calendar.getTime();
	}

	/**
	 * Creates an fingerprint hash of the certificate with the specified hashing
	 * algorithm.
	 * 
	 * @param certificate
	 *            The certificate
	 * @param hashAlgorithm
	 *            The hashing algorithm
	 * @return The fingerprint hash
	 */
	public String getFingerprint(X509Certificate certificate, String hashAlgorithm) {

		try {

			MessageDigest md = MessageDigest.getInstance(hashAlgorithm);
			byte[] data = md.digest(certificate.getEncoded());
			byte[] hexData = Hex.encode(data);
			String hexString = new String(hexData, "ASCII").toUpperCase();

			StringBuffer fp = new StringBuffer();
			int i = 0;
			fp.append(hexString.substring(i, i + 2));
			while ((i += 2) < hexString.length()) {
				fp.append(':');
				fp.append(hexString.substring(i, i + 2));
			}

			return fp.toString();
		} catch (NoSuchAlgorithmException | CertificateEncodingException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Returns the currently specified certificate of the webserver.
	 * 
	 * @return The webserver certificate
	 */
	public CertStoreItem getWebserverCertificate() {

		CertStoreItem certificate = null;

		try {

			KeyStore keyStore = loadKeyStore(configuration.getSsl().getKeyStore().getAbsoluteFile(),
					configuration.getSsl().getKeyStorePass());

			X509Certificate item = (X509Certificate) keyStore.getCertificate("ConnectedCookingServer");
			if (item != null) {

				X500Name x500name = new JcaX509CertificateHolder(item).getSubject();
				RDN cn = x500name.getRDNs(BCStyle.CN)[0];

				certificate = new CertStoreItem();
				certificate.setSerialNumber(item.getSerialNumber().toString());
				certificate.setCommonName(IETFUtils.valueToString(cn.getFirst().getValue()));
				certificate.setSubjectName(item.getSubjectDN().toString());
				certificate.setIssuerName(item.getIssuerDN().toString());
				certificate.setValidFrom(item.getNotBefore());
				certificate.setValidTo(item.getNotAfter());
				certificate.setFingerprintSha1(getFingerprint(item, "SHA-1"));
				certificate.setFingerprintSha256(getFingerprint(item, "SHA256"));
			}

		} catch (KeyStoreException | CertificateEncodingException e) {
			e.printStackTrace();
		}

		return certificate;
	}

	/**
	 * Replaces the current webserver certificate with a new one.
	 * 
	 * @param inputStream
	 *            The input stream of a pkcs12 file
	 * @param password
	 *            The password of the pkcs12 file
	 * @return The new webserver certificate
	 * @throws KeyStoreException
	 *             Will be thrown if the certificate is not valid
	 * @throws UnrecoverableKeyException
	 *             Will be thrown if the provided password is incorrect
	 * @throws IOException
	 *             Will be thrown if the provided file is not in pkcs12 format
	 */
	public CertStoreItem replaceWebserverCertificate(InputStream inputStream, String password)
			throws KeyStoreException, UnrecoverableKeyException, IOException {

		try {

			File keyStoreFile = configuration.getSsl().getKeyStore().getAbsoluteFile();
			String keyStorePass = configuration.getSsl().getKeyStorePass();

			// Get private key and certificate from pfx file
			KeyStore keyFile = KeyStore.getInstance("pkcs12", "SunJSSE");
			keyFile.load(inputStream, password.toCharArray());
			String alias = keyFile.aliases().nextElement();

			X509Certificate publicKey = (X509Certificate) keyFile.getCertificate(alias);
			PrivateKey privateKey = (PrivateKey) keyFile.getKey(alias, password.toCharArray());

			// Replaces the webserver certificate
			KeyStore keyStore = loadKeyStore(keyStoreFile, keyStorePass);
			keyStore.deleteEntry("ConnectedCookingServer");
			keyStore.setKeyEntry("ConnectedCookingServer", privateKey, keyStorePass.toCharArray(),
					new X509Certificate[] { publicKey });
			storeKeyStore(keyStore, keyStoreFile, keyStorePass);

			// Get the certificate details
			CertStoreItem certificate = new CertStoreItem();
			certificate.setSerialNumber(publicKey.getSerialNumber().toString());
			certificate.setSubjectName(publicKey.getSubjectDN().toString());
			certificate.setIssuerName(publicKey.getIssuerDN().toString());
			certificate.setValidFrom(publicKey.getNotBefore());
			certificate.setValidTo(publicKey.getNotAfter());

			return certificate;
		} catch (KeyStoreException | NoSuchProviderException | NoSuchAlgorithmException | CertificateException e) {
			log.error("CertStoreUtil.loadKeyStore()", e);
			throw new KeyStoreException();
		} catch (IOException e) {
			log.error("CertStoreUtil.loadKeyStore()", e);
			if (e.getCause() != null && e.getCause().getClass() == UnrecoverableKeyException.class) {
				throw (UnrecoverableKeyException) e.getCause();
			} else {
				throw e;
			}
		}
	}

	/**
	 * Returns a single trusted certificate by it's alias/serialNumber.
	 * 
	 * @param alias
	 *            The alias/serialNumber
	 * @return The trusted certificate
	 */
	public CertStoreItem getTrustedCertificate(String alias) {

		CertStoreItem certificate = null;

		try {

			KeyStore trustStore = loadKeyStore(configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass());

			X509Certificate item = (X509Certificate) trustStore.getCertificate(alias);
			if (item != null) {

				X500Name x500name = new JcaX509CertificateHolder(item).getSubject();
				RDN cn = x500name.getRDNs(BCStyle.CN)[0];

				certificate = new CertStoreItem();
				certificate.setSerialNumber(item.getSerialNumber().toString());
				certificate.setCommonName(IETFUtils.valueToString(cn.getFirst().getValue()));
				certificate.setSubjectName(item.getSubjectDN().toString());
				certificate.setIssuerName(item.getIssuerDN().toString());
				certificate.setValidFrom(item.getNotBefore());
				certificate.setValidTo(item.getNotAfter());
				certificate.setFingerprintSha1(getFingerprint(item, "SHA-1"));
				certificate.setFingerprintSha256(getFingerprint(item, "SHA256"));
			}

		} catch (KeyStoreException | CertificateEncodingException e) {
			log.error("CertStoreUtil.removeTrustedCertificate()", e);
		}

		return certificate;
	}

	/**
	 * Returns a list of all trusted certificates.
	 * 
	 * @return The list of certificates
	 */
	public List<CertStoreItem> getTrustedCertificates() {

		List<CertStoreItem> certificates = new LinkedList<>();

		try {

			KeyStore trustStore = loadKeyStore(configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass());

			Enumeration<String> aliases = trustStore.aliases();
			while (aliases.hasMoreElements()) {
				X509Certificate certificate = (X509Certificate) trustStore.getCertificate(aliases.nextElement());

				CertStoreItem item = new CertStoreItem();

				X500Name x500name = new JcaX509CertificateHolder(certificate).getSubject();
				RDN cn = x500name.getRDNs(BCStyle.CN)[0];

				item.setSerialNumber(certificate.getSerialNumber().toString());
				item.setCommonName(IETFUtils.valueToString(cn.getFirst().getValue()));
				item.setSubjectName(certificate.getSubjectDN().toString());
				item.setIssuerName(certificate.getIssuerDN().toString());
				item.setValidFrom(certificate.getNotBefore());
				item.setValidTo(certificate.getNotAfter());
				item.setFingerprintSha1(getFingerprint(certificate, "SHA-1"));
				item.setFingerprintSha256(getFingerprint(certificate, "SHA256"));
				certificates.add(item);
			}

		} catch (KeyStoreException | CertificateEncodingException e) {
			log.error("CertStoreUtil.removeTrustedCertificate()", e);
		}

		return certificates;
	}

	/**
	 * Adds a certificate to the list of trusted certificates.
	 * 
	 * @param inputstream
	 *            The input stream of the certificate
	 * @return The new certificate or null
	 */
	public CertStoreItem addTrustedCertificate(InputStream inputstream) {

		try {

			KeyStore trustStore = loadKeyStore(configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass());

			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(inputstream);
			trustStore.setCertificateEntry(certificate.getSerialNumber().toString(), certificate);

			if (storeKeyStore(trustStore, configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass())) {
				CertStoreItem item = new CertStoreItem();
				item.setSerialNumber(certificate.getSerialNumber().toString());
				item.setSubjectName(certificate.getSubjectDN().toString());
				item.setIssuerName(certificate.getIssuerDN().toString());
				item.setValidFrom(certificate.getNotBefore());
				item.setValidTo(certificate.getNotAfter());

				reloadTrustStore();
				return item;
			}
		} catch (KeyStoreException | CertificateException e) {
			log.error("CertStoreUtil.removeTrustedCertificate()", e);
		}

		return null;
	}

	/**
	 * Removes the certificate with the specified alias from the trusted
	 * certificates.
	 * 
	 * @param alias
	 *            The alias/serialNumber
	 * @return True if successful or false if not
	 */
	public boolean removeTrustedCertificate(String alias) {

		try {

			KeyStore trustStore = loadKeyStore(configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass());
			trustStore.deleteEntry(alias);

			if (storeKeyStore(trustStore, configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass())) {
				reloadTrustStore();
				return true;
			}
		} catch (KeyStoreException e) {
			log.error("CertStoreUtil.removeTrustedCertificate()", e);
		}

		return false;
	}

	/**
	 * Reloads the truststore and merges the default SSL Context with the
	 * application SSL Context to reload the certificate store.
	 */
	public void reloadTrustStore() {

		try {

			KeyStore trustStore = loadKeyStore(configuration.getTrustStore().getAbsoluteFile(),
					configuration.getTrustStorePass());

			// Get hold of the default trust manager
			X509TrustManager defaultTm = getDefaultTrustManager(null);
			X509TrustManager customTm = getDefaultTrustManager(trustStore);

			CertStoreTrustManager trustManager = new CertStoreTrustManager(defaultTm, customTm);

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[] { trustManager }, null);

			SSLContext.setDefault(sslContext);

		} catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
			log.error("CertStoreUtil.reloadTrustStore()", e);
		}
	}

	/**
	 * Creates an initial keystore with the intial generated webserver certificate.
	 * 
	 * @param keyStorePassword
	 *            The keystore password
	 * @param keyLength
	 *            The length of the private key
	 * @return True if successful or false if not
	 */
	public boolean initKeyStore(String keyStorePassword, int keyLength) {

		FileOutputStream outputStream = null;

		try {

			KeyPair keyPair = generateKeyPair(keyLength);
			X509Certificate certificate = createCertificate(keyPair);

			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(null, keyStorePassword.toCharArray());
			keyStore.setKeyEntry("ConnectedCookingServer", keyPair.getPrivate(), keyStorePassword.toCharArray(),
					new X509Certificate[] { certificate });

			File keyStoreFile = new File("conf/keystore");
			if (!keyStoreFile.getAbsoluteFile().getParentFile().exists()) {
				keyStoreFile.getAbsoluteFile().getParentFile().mkdirs();
			}

			outputStream = new FileOutputStream(keyStoreFile.getAbsoluteFile());
			keyStore.store(outputStream, keyStorePassword.toCharArray());

			return true;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | OperatorCreationException
				| IOException e) {
			log.error("CertStoreUtil.initKeyStore()", e);
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
			}
		}

		return false;
	}

	/**
	 * Creates an initial truststore with no entries.
	 * 
	 * @param trustStorePassword
	 *            The password of the trust store.
	 * @return True if successful or false if not
	 */
	public boolean initTrustStore(String trustStorePassword) {

		FileOutputStream outputStream = null;

		try {

			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, trustStorePassword.toCharArray());

			File keyStoreFile = new File("conf/truststore");
			if (!keyStoreFile.getAbsoluteFile().getParentFile().exists()) {
				keyStoreFile.getAbsoluteFile().getParentFile().mkdirs();
			}

			outputStream = new FileOutputStream(keyStoreFile.getAbsoluteFile());
			trustStore.store(outputStream, trustStorePassword.toCharArray());

			return true;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			log.error("CertStoreUtil.initTrustStore()", e);
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
			}
		}

		return false;
	}

}