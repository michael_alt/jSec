package org.jsec.core.security.cert;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * This class is used to merge the default trustmanager with the application
 * specific trustmanager.
 * 
 * @author alt
 *
 */
public final class CertStoreTrustManager implements X509TrustManager {

	private final X509TrustManager defaultTrustManager;
	private final X509TrustManager customTrustManager;

	public CertStoreTrustManager(X509TrustManager defaultTrustManager, X509TrustManager customTrustManager) {
		this.defaultTrustManager = defaultTrustManager;
		this.customTrustManager = customTrustManager;
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return this.defaultTrustManager.getAcceptedIssuers();
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		try {
			this.customTrustManager.checkServerTrusted(chain, authType);
		} catch (CertificateException e) {
			this.defaultTrustManager.checkServerTrusted(chain, authType);
		}
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		this.defaultTrustManager.checkClientTrusted(chain, authType);
	}

}