package org.jsec.core.security.cert;

import java.util.Date;

/**
 * This class represents a single key- or truststore certificate.
 * 
 * @author alt
 *
 */
public class CertStoreItem {

	private String serialNumber;
	private String commonName;
	private String subjectName;
	private String issuerName;

	private Date validFrom;
	private Date validTo;

	private String fingerprintSha1;
	private String fingerprintSha256;

	public CertStoreItem() {
		// The default constructor
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getFingerprintSha1() {
		return fingerprintSha1;
	}

	public void setFingerprintSha1(String fingerprintSha1) {
		this.fingerprintSha1 = fingerprintSha1;
	}

	public String getFingerprintSha256() {
		return fingerprintSha256;
	}

	public void setFingerprintSha256(String fingerprintSha256) {
		this.fingerprintSha256 = fingerprintSha256;
	}

}