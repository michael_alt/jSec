package org.jsec.core.security.password;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.jsec.core.util.StringEncodingUtils;
import org.jsec.core.util.validation.ArrayUtils;

/**
 * This class is used to manage user passwords.
 * 
 * @author alt
 *
 */
public class PasswordEncoder {

	private static final String PASSWORD_HASH_ALGORITHM = "PBKDF2WithHmacSHA512";
	private static final int PASSWORD_SALT_LENGTH = 64;
	private static final int PASSWORD_HASH_ITERATIONS = 200000;
	private static final int PASSWORD_HASH_LENGTH = 256;

	/**
	 * The default constructor of the class.
	 */
	public PasswordEncoder() {
		// Default constructor
	}

	/**
	 * Encode the raw password.
	 * 
	 * @param password
	 *            The raw password
	 * @return The encoded password
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the hashing algorithm is no available
	 * @throws InvalidKeySpecException
	 *             Will be thrown if the key algorithm is not available
	 */
	public String encode(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {

		byte[] salt = generateRandomSalt();
		byte[] encoded = encodePassword(password, salt);

		return StringEncodingUtils.encodeBase64(encoded);
	}

	/**
	 * Verify that the stored password matches the submitted raw password.
	 * 
	 * @param rawPassword
	 *            The raw password attempt
	 * @param encodedPassword
	 *            The stored password hash
	 * @return True if the passwords match, false if not
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the hashing algorithm is no available
	 * @throws InvalidKeySpecException
	 *             Will be thrown if the key algorithm is not available
	 */
	public boolean verify(String rawPassword, String encodedPassword)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		byte[] encoded = StringEncodingUtils.decodeBase64(encodedPassword);
		byte[] salt = ArrayUtils.subArray(encoded, 0, PasswordEncoder.PASSWORD_SALT_LENGTH);

		return matches(encoded, encodePassword(rawPassword, salt));
	}

	/**
	 * Generates the strongest secure random for the current platform.
	 * 
	 * @return The random salt
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the hashing algorithm is no available
	 */
	private byte[] generateRandomSalt() throws NoSuchAlgorithmException {
		return SecureRandom.getInstanceStrong().generateSeed(PasswordEncoder.PASSWORD_SALT_LENGTH);
	}

	/**
	 * Creates an one way hash of the provided cleartext password.
	 * 
	 * @param password
	 *            The cleartext password
	 * @param passwordSalt
	 *            The random password salt
	 * @return The hashed password
	 * @throws NoSuchAlgorithmException
	 *             Will be thrown if the hashing algorithm is no available
	 * @throws InvalidKeySpecException
	 *             Will be thrown if the provided passphrase is to week
	 */
	private byte[] encodePassword(String password, byte[] passwordSalt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(PASSWORD_HASH_ALGORITHM);
		SecretKey key = keyFactory.generateSecret(
				new PBEKeySpec(password.toCharArray(), passwordSalt, PASSWORD_HASH_ITERATIONS, PASSWORD_HASH_LENGTH));

		return ArrayUtils.concat(passwordSalt, key.getEncoded());
	}

	/**
	 * Constant time comparison to prevent against timing attacks.
	 * 
	 * @param expected
	 *            The expected byte array
	 * @param actual
	 *            The byte array of the password attempt
	 * @return True if the arrays match
	 */
	private static boolean matches(byte[] expected, byte[] actual) {

		if (expected.length != actual.length) {
			return false;
		}

		int result = 0;
		for (int index = 0; index < expected.length; index++) {
			result |= expected[index] ^ actual[index];
		}

		return result == 0;
	}

}