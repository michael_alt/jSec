package org.jsec.core.exceptions;

import org.jsec.core.util.error.ApplicationError;

/**
 * This exception will be thrown if an entity could not be found.
 * 
 * @author alt
 *
 */
public class EntityNotFoundException extends Exception {

	private static final long serialVersionUID = 8868831374306844969L;
	private ApplicationError error;

	public EntityNotFoundException() {
		super();
	}

	public EntityNotFoundException(String message) {
		super(message);
	}

	public EntityNotFoundException(ApplicationError error) {
		super(error.getMessage());
		this.error = error;
	}

	public EntityNotFoundException(Exception exception) {
		super(exception);
	}

	public ApplicationError getError() {
		return this.error;
	}

}