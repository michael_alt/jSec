package org.jsec.core.exceptions;

import org.jsec.core.util.error.ApplicationError;

/**
 * This exception will be thrown if an database query error occurs.
 * 
 * @author alt
 *
 */
public class EntityPersistenceException extends Exception {

	private static final long serialVersionUID = 4126495538617470708L;
	private ApplicationError error;

	public EntityPersistenceException() {
		super();
	}

	public EntityPersistenceException(String message) {
		super(message);
	}

	public EntityPersistenceException(ApplicationError error) {
		super(error.getMessage());
		this.error = error;
	}

	public EntityPersistenceException(Exception exception) {
		super(exception);
	}

	public ApplicationError getError() {
		return this.error;
	}

}