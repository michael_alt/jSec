package org.jsec.core.exceptions;

/**
 * This exception will be thrown if an error during the loading/validation of
 * the application configuration file occurs.
 * 
 * @author alt
 *
 */
public class ConfigurationValidationException extends Exception {

	private static final long serialVersionUID = 4879534014576321948L;

	public ConfigurationValidationException() {
		super();
	}

	public ConfigurationValidationException(String message) {
		super(message);
	}

	public ConfigurationValidationException(Exception exception) {
		super(exception);
	}

}