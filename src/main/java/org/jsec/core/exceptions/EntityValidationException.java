package org.jsec.core.exceptions;

import org.jsec.core.util.error.ApplicationError;

/**
 * This exception will be thrown if an invalid data object was provided.
 * 
 * @author alt
 *
 */
public class EntityValidationException extends Exception {

	private static final long serialVersionUID = -1286622399635353449L;
	private ApplicationError error;

	public EntityValidationException() {
		super();
	}

	public EntityValidationException(String message) {
		super(message);
	}

	public EntityValidationException(ApplicationError error) {
		super(error.getMessage());
		this.error = error;
	}

	public EntityValidationException(Exception exception) {
		super(exception);
	}

	public ApplicationError getError() {
		return this.error;
	}

}