package org.jsec.core.exceptions;

import org.jsec.core.util.error.ApplicationError;

/**
 * This exception will be thrown if an entity with the same unique identifier
 * already exists.
 * 
 * @author alt
 *
 */
public class EntityDuplicateException extends Exception {

	private static final long serialVersionUID = -9104389886314758892L;
	private ApplicationError error;

	public EntityDuplicateException() {
		super();
	}

	public EntityDuplicateException(String messsage) {
		super(messsage);
	}

	public EntityDuplicateException(ApplicationError error) {
		super(error.getMessage());
		this.error = error;
	}

	public EntityDuplicateException(Exception exception) {
		super(exception);
	}

	public ApplicationError getError() {
		return this.error;
	}

}